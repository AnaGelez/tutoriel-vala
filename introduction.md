Bonjour à tous.

Ce tutoriel a pour but de vous apprendre à programmer en **Vala**. Vala est un langage de programmation créé par les membres du projet GNOME (un environnement de bureau très répandu sous GNU/Linux), mais cela ne veut pas dire qu'il est limité à la création d'applications pour celui-ci.

Ce tutoriel et idéal si vous n'avez jamais programmé, et que vous voulez vous essayer à la création d'applications. Nous commencerons par faire des choses très simples, mais à la fin de ce tutoriel, vous serez capable de créer des applications à l'architecture assez complexe, et capable de faire à peu près tout ce que vous voudrez.

Vala est un langage qui permet de créer beaucoup de choses différentes. Vous pouvez en effet créer des applications pour ordinateur (qui fonctionneront sur Windows, GNU/Linux et Mac OS X), des sites web avec Valum ou Valse, l'utiliser pour de l'Internet des objets grâce à vala-avr, ou encore créer une application Android[^android].

[[i]]
| **Prérequis**    
| Disposer d'un ordinateur et savoir faire des manipulations simples avec (installation de programmes ...).
|
| **Objectifs**    
| Apprendre à programmer avec le langage Vala.     
| Comprendre ce qu'est la programmation orientée objet.

[^android]: En réalité, le support est très basique, et Vala n'est pas le langage idéal pour créer une application Android, même si c'est possible.