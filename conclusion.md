Ce tutoriel n'est pas terminé, d'autres parties vont bientôt arriver !

Nous voudrions remercier Taurre, qui a validé ce contenu ainsi que Wizix, WinXaito, Praetonus, Emeric, mrBen et skyzohkey qui nous ont aidé pendant la phase de bêta à rendre ce tutoriel meilleur.

D'autres tutoriels pour vous apprendre à utiliser Vala plus concrètement sont en cours de préparation, et devraient bientôt arriver. En attendant, il y a toujours [la documentation officielle](http://valadoc.org) qui peut être un bon début pour apprendre à utiliser les différentes bibliothèques.