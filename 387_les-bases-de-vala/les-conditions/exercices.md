On vient d'apprendre beaucoup de nouvelles choses, maintenant c'est l'heure de la pratique ! :pirate:

# Exercice 1

Ce premier exercice est assez simple : il s'agit de déterminer si une année entrée par l'utilisateur est bissextile ou non. Une année est bissextile si elle est divisible par 4 mais pas par 100, ou qu'elle est divisible par 400.

[[information]]
| Pour savoir si `a` est divisible par `b`, il faut regarder si `a % b` vaut 0.

À vous de jouer !

## Correction

Avant de vous jetez sur la réponse, nous vous rappellons que regarder la correction n'a aucun intérêt si vous n'avez pas essayé d'écrire votre propre programme. Si vous ne pratiquez pas, vous ne progresserez pas. ;)

[[secret]]
| ```vala
| void main () {
|     print ("Entrez une année : ");
|     int annee = int.parse (stdin.readline ());
|     if ((annee % 4 == 0 && annee % 100 != 0) || annee % 400 == 0) {
|         print ("Cette année est bissextile.\n");
|     } else {
|         print ("Cette année n'est pas bissextile.\n");
|     }
| }
| ```

Si vous n'avez pas exactement le même code, ce n'est pas grave du moment que votre programme fonctionne correctement. :)

# Exercice 2

Cet exercice va nous permettre de manipuler la structure `switch`. Votre but va être de saluer l'utilisateur différemment en fonction de sa langue (qu'il choisit, bien-sûr ;) ). Plus vous supporterez de langues, mieux ça sera.

## Correction

[[secret]]
| ```vala
| void main () {
|     print ("Langues disponibles : \nfr, en, es, de, eo, it\n");
|     print ("Quelle langue parlez-vous ? ");
|     string langue = stdin.readline ();
|     switch (langue) {
|         case "fr":
|             print ("Bonjour !\n");
|             break;
|         case "en":
|             print ("Hello!\n");
|             break;
|         case "es":
|             print ("¡ Hola !\n");
|             break;
|         case "de":
|             print ("Hallo!\n");
|             break;
|         case "eo":
|             print ("Saluton !\n");
|             break;
|         case "it":
|             print ("Ciao !\n");
|             break;
|         default:
|             print ("Langue invalide.\n");
|             break;
|     }
| }
| ```