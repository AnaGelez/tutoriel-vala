Nous avons vu lors du chapitre précédent que les booléens étaient un type de variable qui ne pouvait prendre que deux valeurs : `true` (vrai) ou `false` (faux). Ce type va être très utile dans la création de conditions et de boucles (nous verrons au chapitre suivant ce qu'est une boucle, ne vous inquiétez pas ^^ ). On peut donc écrire le code suivant :

```vala
bool zds_cest_bien = vrai, je_deteste_vala = false;
```

Là où les booléens vont devenir plus intéressants, c'est avec l'utilisation **d'opérateurs de comparaison**. On va écrire une expression qui compare deux valeurs grâce à ces opérateurs, et ont pourra stocker le résultat dans un booléen. En Vala, les opérateurs de comparaison sont :

| Opérateur | Signification               |
|-----------|-----------------------------|
| `==`      | est égal à                  |
| `!=`      | est différent de            |
| `<`       | est strictement inférieur à |
| `>`       | est strictement supérieur à |
| `<=`      | est inférieur ou égal à     |
| `>=`      | est supérieur ou égal à     |

Faites bien attention : `=` assigne une valeur à une variable, alors que `==` vérifie l'égalité entre deux valeurs. Une erreur assez courante lorsqu'on débute en programmation est de les confondre. De même, `=>` n'a pas la même signification que `>=`, mais nous ne découvrirons que plus tard à quoi il sert. ;)

[[information]]
| Il n'existe pas de code pour insérer un booléen dans une chaîne de format. On peut cependant faire `mon_booleen.to_string ()` pour obtenir une valeur de type `string` que l'on pourra ensuite afficher avec `%s`.

Ces opérateurs marchent sur tous les types de nombres. Pour les chaînes de caractères, on ne peut utiliser que `==` et `!=`. Pour les utiliser, il faut simplement encadrer l'opérateur par les deux valeurs (ou variables) que l'on veut comparer. Il faut cependant que les deux valeurs soient du même type. Par exemple, ce code regarde si 7 est plus grand que 42 et stocke le résultat dans la variable `comparaison`, qui vaudra donc `false`.

```vala
bool comparaison = 7 > 42;
```

Maintenant, on va voir si vous avez compris. Voici des petits programmes. À chaque fois essayez de devinez ce qu'ils afficheront.

```vala
bool resultat = 10 > 15; // On fait une comparaison.
print ("%s\n", resultat.to_string ()); // On affiche le résultat, mais il faut d'abord le convertir en `string`.
```

[[secret]]
| On verra `false`.

```vala
bool resultat = "Bonjour" == "Bonjour";
print ("%s\n", resultat.to_string ());
```

[[secret]]
| On aura `true`.

```vala
int nombre = int.parse (stdin.read_line ());
bool resultat = nombre == 42;
print ("%s\n", resultat.to_string ());
```

[[secret]]
| Ici, tout dépends de ce que l'on va entrer. Si on entre `42`, on verra `true`, sinon on verra `false`.

```vala
bool resultat = "5" == 5;
print ("%s\n", resultat.to_string ());
```

[[secret]]
| Ce programme… provoquera une erreur de compilation car on ne peut pas comparer une chaîne et un entier. :D

# Combiner les comparaisons

Vala nous offre aussi la possibilité de combiner le résultat de plusieurs comparaisons en un seul ou d'inverser un résultat, avec les **connecteurs logiques**. En Vala, ils sont trois :

- Le ET logique, écrit `&&` ;
- Le OU logique, noté `||` ;
- Le NON logique, écrit `!`;

## Le ET logique

Cet opérateur sert à combiner deux comparaisons (appelées *opérandes*), en se plaçant entre elles. Le résultat final sera `true` que si les deux opérandes valent aussi `true`. Sinon, ça sera `false`. On l'écrit `&&`. Prenons un exemple.

```vala
int x = int.parse (stdin.readline ()); // on récupère un entier entré par l'utilisateur
bool resultat = 10 > x && x != 5;
print ("resultat vaut %s\n", resultat.to_string ());
```

Ici, notre programme agira différemment selon ce qu'on choisir comme nombre. Si on entre un nombre strictement inférieur à 10 **et** différent de 5, alors `resultat` vaudra `true`. Dans tous les autres cas, il aura pour valeur `false` (si on rentre 5 ou un nombre supérieur ou égal à 10).

TODO: détailler un peu ça - On peut aussi écrire plusieurs combinaisons à la suite, mais il faut savoir qu'elles s'effectueront de la gauche vers la droite.

```vala
bool resultat = x != 5 && x != 8 && x > 10;
```

## Le OU logique 

Le OU logique fonctionne de la même façon que le ET logique, à ceci près que le résultat vaut `true` à partir du moment où une des deux opérandes a pour valeur `true`. En Vala, on utilise `||` pour noter un OU logique (faites ||alt gr||+||6|| si vous avez un clavier AZERTY).

```vala
// Est-ce que je dois mettre mon manteau avant de sortir ?
bool manteau = temperature < 20 || temps == "pluie";
```

Ici on définit une variable `manteau` qui vaut `true` si la température vaut moins de 20^o^C ou s'il pleut. Bien sûr, si ces deux conditions sont remplies toutes les deux à la fois, on met aussi notre manteau.

## Le NON logique

Ce dernier connecteur s'applique à un seul booléen (on dit qu'il est unaire), et en inverse le résultat. Il est noté `!` et se place devant le booléen à inverser. Pour être sûr que le compilateur prenne seulement notre booléen, ni plus ni moins, on va généralement l'encadrer de parenthèses.

```vala
bool resultat = !(x == 5 && y == 8);
```

Dans l'exemple ci-dessus, pour que `resultat` ait pour valeur `true`, il faut que `x` soit différent de 5 ou que `y` soit différent de 8 (ou les deux à la fois). Finalement on aurait pu simplement utiliser ce code.

```vala
bool resultat = x != 5 || y != 8;
```

Mais, pour certaines opérations que nous découvrirons par la suite, il n'existe pas d'opération inverse. C'est donc surtout à ce moment là que le `!` nous sera utile, même si il peut déjà être plus clair à lire.

# Petit bonus : enchaîner les opérateurs

Vala nous permet également d'enchaîner les opérateurs de comparaison. Bien que cette fonctionnalité soit encore un peu expérimentale (ne vous inquiétez donc pas si le compilateur vous affiche un message d'avertissement), elle est très pratique. Ainsi, ces deux expressions sont équivalentes, mais la deuxième est bien plus lisible.

```vala
bool sans_enchainer = a < b && b < c && c < d && d < e;

bool en_enchainant = a < b < c < d < e;
```