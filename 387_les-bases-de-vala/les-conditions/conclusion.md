Vos programmes peuvent donc maintenant s'exécuter différemment en fonction des valeurs de certaines variables, et ils deviennent tout de suite beaucoup intéressants. Voici, en résumé, ce qu'il faut retenir de ce chapitre.

- Les booléens sont des variables qui n'ont que deux valeurs possibles : `true` ou `false`. Ils sont utilisés dans les **structures conditionnelles** comme **prédicats** ;
- On peut comparer des variables avec les opérateurs suivant `<`, `<=`, `>`, `>=`, `==` et `!=`. On peut combiner plusieurs comparaisons avec `&&` et `||`, et inverser leur valeur avec `!` ; 
- La structure `if` sert à exécuter des instructions sous certaines conditions. On peut l'accompagner d'un `else` contenant les instructions à exécuter si cette condition n'est pas vérifiée, et d'un ou plusieurs `else if` qui s'exécuteront si les précédents `if` et `else if` ne l'ont pas été, et si leur prédicat vaut `true` ;
- La structure `switch` permet d'exécuter des actions en fonction de la valeur d'une variable, mais seulement dans certains de cas prédéfinis ;
- Les expressions ternaires sont des structures conditionnelles qui tiennent en une ligne.