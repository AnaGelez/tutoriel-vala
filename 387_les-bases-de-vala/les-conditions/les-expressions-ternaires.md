Une **expression ternaire** est une structure conditionnelle qui tient en une seule ligne[^ligne] ! Elles sont parfois un peu difficiles à lire lorsqu'on y est pas habitués, mais elles sont très pratiques. Ces expressions vont prendre une valeur qui dépend d'un prédicat.

Pour écrire une expression ternaire, on utilise cette syntaxe.

```vala
predicat ? valeur_si_vrai : valeur_si_faux;
```

On peut par exemple les utiliser pour mettre un mot au pluriel.

```vala
int utilisateurs = 3;
print ("Il y a %d %s connectés.\n", utilisateurs, utilisateurs < 2 ? "utilisateur" : "utilisateurs");
```

Essayez ce programme plusieurs fois, en changeant `utilisateurs` entre chaque compilation.

On va analyser un peu plus en détail ce qui se passe dans cet exemple, surtout au niveau de l'expression ternaire. Ici, le prédicat est `utilisateurs < 2`. Il sera donc vrai si la variable `utilisateurs` est strictement inférieure à 2. Or, si il est vrai, l'expression ternaire aura pour valeur `"utilisateur"`, au singulier donc. Si au contraire il vaut `false`, on le met au pluriel : l'expression aura pour valeur `"utilisateurs"`.

[^ligne]: En réalité, on peut écrire un `if` sur une seule ligne, mais ça n'est pas très lisible.