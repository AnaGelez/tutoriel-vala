Maintenant qu'on a vu comment fonctionnait les booléens plus en détails, on va pouvoir s'intéresser aux conditions. Les conditions (ou structures conditionnelles pour être exact) sont des structures qui vont nous permettre d'exécuter certaines instructions seulement dans certains cas. Par exemple, on peut afficher un messages secret si l'utilisateur a entré le bon mot de passe ou envoyer un message uniquement si celui-ci fait bien moins de 140 caractères.

La première structure conditionnelle (ou simplement condition si vous préférez :p ) que nous allons voir est la structure `if` (« if » signifie « si » en Anglais). C'est un bloc[^bloc] qui se construit de cette façon.

```vala
if (predicat) {
    
}
```

Ici, `predicat` doit être un booléen. Mais en général, on écrit directement une expression, plutôt que de passer par une variable intermédiaire. Toutes les instructions qui seront à l'intérieur de ce bloc ne seront exécutées que si `predicat` vaut `true`.

[[information]]
| **Prédicat** est le terme technique pour dire « condition ».

Et si on créait un petit système de protection par mot de passe ?

```vala
const string mot_de_passe = "1234"; // le mot de passe attendu
print ("Entrez votre mot de passe : ");
string saisie = stdin.readline (); // on lit le mot de passe

// on regarde si le mot de passe entré correspond au vrai mot de passe
if (saisie == mot_de_passe) {
    print ("Mot de passe correct !\n");
}
```

Ce programme a deux exécution possibles : soit on entre le bon mot de passe, et un message s'affiche, soit on se trompe et il s'arrête sans rien nous dire de plus.

On peut cependant effectuer des actions lorsque que le prédicat du `if` vaut `false`. On va pour cela utiliser le mot-clé `else`, qui signifie « sinon » en Anglais, qu'on va placer juste après la fin du bloc du `if` correspondant. On va ensuite ouvrir un bloc qui contiendra les instructions à exécuter si le prédicat n'est pas vérifié. Notre petit programme peut donc être amélioré de cette façon.

```vala
const string mot_de_passe = "1234";
print ("Entrez votre mot de passe : ");
string saisie = stdin.readline ();

if (saisie == mot_de_passe) { // si le mot de passe est valide
    print ("Mot de passe correct !\n");
} else { // sinon...
    print ("Mot de passe incorrect !\n");
}
```

Compilez ce programme et lancez-le, puis entrez un mot de passe invalide, vous devriez cette fois voir un message qui vous indique que vous vous êtes trompés. ;)

On peut même combiner le `if` et le `else` en un `else if`. Tout comme le `if`, il ne s'exécutera que si son prédicat est vrai, mais il n'aura une chance de s'exécuter que si le prédicat du `if` correspondant n'est pas vérifiée, comme pour le `else`. Ce bloc va s'introduire juste après celui du `if` et avant l'éventuel `else`. Notre système d'authentification va pouvoir supporter plusieurs utilisateurs !

```vala
// les mots de passes de nos deux utilisateurs
const string mot_de_passe_jacques = "1234";
const string mot_de_passe_justine = "motdepasse";

// on lit le mot de passe entré
print ("Entrez votre mot de passe : ");
string saisie = stdin.readline ();

if (saisie == mot_de_passe_jacques) { // c'est le mot de passe de Jacques
    print ("Bonjour Jacques !\n");
} else if (saisie == mot_de_passe_justine) { // c'est celui de Justine
    print ("Bienvenue Justine !\n");
} else { // Ce n'est le mot de passe de personne
    print ("Mot de passe incorrect !\n");
}
```

Vous pouvez mettre autant de blocs `else if` que vous voulez, sachez simplement qu'ils seront vérifiés de haut en bas, et que dès qu'un sera exécuté, notre programme ne vérifiera pas les autres.

[[information]]
| Petit rappel : les `else` et `else if` sont complètement optionnels : on avait écrit un bloc `if` seul, et notre code fonctionnait très bien.

[^bloc]: Nous verrons plus en détail ce que sont les blocs à la fin de ce chapitre, pour le moment sachez simplement que c'est une suite d'instructions entre accolades.