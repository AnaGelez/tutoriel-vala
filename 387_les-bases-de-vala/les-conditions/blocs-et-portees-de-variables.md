Peut être avez vous essayé de compiler un programme similaire à celui-ci.

```vala
void main () {
    int age = int.parse (stdin.readline ());
    if (age >= 18) {
        string message = "Vous êtes majeur.\n";
    } else {
        string message = "Vous êtes mineur.\n";
    }
    print (message);
}
```

Mais vous avez alors du recevoir une erreur de la part du compilateur.

[[erreur]]
| The name \`message' does not exist in the context of \`main'

Pour comprendre pourquoi ce programme ne fonctionne pas, il va falloir parler des blocs et de la portée des variables.

# Les blocs

En Vala, un bloc est une suite d'instructions contenues entre accolades. Ainsi, après le prédicat d'un `if`, on ouvre un nouveau bloc, dont on ressort lorsqu'on rencontre le `}` correspondant.

```vala
if (a < x) { // On ouvre un nouveau bloc
    print ("a est plus petit que x.");
} // On en ressort
```

Or il faut savoir que les variables définies dans un bloc ne sont accessibles que dans ce bloc et les blocs enfants. Donc dans notre exemple précédent, quand on déclarait la variable `message` dans les blocs du `if` et du `else`, elle n'était pas accessible dans le bloc parent, celui du `main`. Il aurait donc fallu faire quelque chose comme ça.

```vala
void main () {
    int age = int.parse (stdin.readline ());
    string message = "Vous êtes mineur.\n"; // message est bien déclaré dans le bloc du main
    if (age >= 18) {
        string message = "Vous êtes majeur.\n";
    }
    // Si on n'entre pas dans la condition, le message reste celui par défaut, donc on pas besoin d'un else.
    print (message);
}
```

# Indenter son code

Vous avez peut-être remarqué que nous décalions notre code à l'intérieur des blocs. C'est ce qu'on appelle l'**indentation**. On distingue ainsi clairement là où commencent et finissent les différents blocs.

On peut soit utiliser des espaces (deux ou quatre en général) ou des tabulations pour indenter son code. L'important est de rester cohérent et de toujours indenter de la même façon dans tout votre code.

Les niveaux d'indentation s'ajoutent les uns aux autres. Si vous avez deux blocs imbriqués vous décalerez plus dans le second que dans le premier.

```vala
void main () {
    print ("Indenté avec quatre espaces.\n");
    if (true) {
        print ("Indenté avec deux fois quatre espaces.\n");
    } 
}
```

Vous remarquerez d'ailleurs cette condition extrêmement utile…