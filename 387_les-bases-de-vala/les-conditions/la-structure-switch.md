Imaginez que vous deviez trouver la valeur d'une variable parmi plusieurs valeurs possibles. Vous feriez sûrement un grand nombre de `else if`, pas très lisible. Or la lisibilité de votre code est quelque chose de très important.

On va donc utiliser une nouvelle structure, plus adaptée à ce genre de situations… le `switch`. On va l'écrire de cette façon.

```vala
switch (variable) {

}
```

Ici, `variable` est la variable dont on veut trouver la valeur. Le bloc du `switch` va contenir les actions à effectuer selon la valeur de cette variable. Pour faire correspondre des actions à une valeur, on utilise cette syntaxe, qu'on va répéter pour chaque valeur possible.

```vala
case valeur:
    // Instructions
    break;
```

[[attention]]
| N'oubliez surtout pas l'instruction `break;` à la fin, sinon les cas suivant seront eux aussi exécutés.

On peut par exemple écrire un petit programme qui affiche un message différent en fonction du jour de la semaine.

```vala
print ("Quel jour sommes nous ? ");
string jour = stdin.readline ();
switch (jour) {
    case "Lundi":
        print ("Au travail !\n");
        break;
    case "Mardi":
        print ("Au travail !\n");
        break;
    case "Mercredi":
        print ("Au travail !\n");
        break;
    case "Jeudi":
        print ("Au travail !\n");
        break;
    case "Vendredi":
        print ("Au travail !\n");
        break;
    case "Samedi":
        print ("C'est le week-end !\n");
        break;
    case "Dimanche":
        print ("C'est le week-end !\n");
        break;
}
```

Bon, là notre programme est un peu chargé mais surtout on répète plusieurs fois la même opération dans des cas différents. Heureusement, on peut lier les mêmes instructions à plusieurs cas différents. Il suffit de mettre tous les `case valeur:` à la suite, sans instructions, `break`, sauf pour le dernier.

Notre code devient alors…

```vala
print ("Quel jour sommes nous ? ");
string jour = stdin.readline ();
switch (jour) {
    case "Lundi":
    case "Mardi":
    case "Mercredi":
    case "Jeudi":
    case "Vendredi":
        print ("Au travail !\n");
        break;
    case "Samedi":
    case "Dimanche":
        print ("C'est le week-end !\n");
        break;
}
```

C'est déjà beaucoup mieux, mais ce programme a encore un problème : que ce passe-t-il si on n'entre pas un jour valide ? Notre programme ne va tout simplement rien afficher de plus. Pour remédier à ça, il existe un mot-clé qui décrit le cas par défaut : `default`. On va l'utiliser un peu de la même façon qu'un `case`.

```vala
default:
    // Instructions
    break;
```

Notre programme peut donc devenir

```vala
print ("Quel jour sommes nous ? ");
string jour = stdin.readline ();
switch (jour) {
    case "Lundi":
    case "Mardi":
    case "Mercredi":
    case "Jeudi":
    case "Vendredi":
        print ("Au travail !\n");
        break;
    case "Samedi":
    case "Dimanche":
        print ("C'est le week-end !\n");
        break;
    default:
        print ("%s n'est pas un jour de la semaine.\n", jour);
        break;
}
```

Cette syntaxe, bien qu'avantageuse dans certains cas précis, est un peu moins utilisée que la structure `if`/`else` car il faut décrire chaque cas possible.

[[information]]
| Les conventions de Vala veulent que, lorsque c'est possible, vous préfériez toujours un `switch` à un `if`/`else` contenant plus de deux blocs `else if`.