Faire retourner une valeur à une méthode va permettre d'assigner cette valeur à une variable. Ainsi, vous pourrez par exemple traduire un texte et obtenir la traduction. C'est d'ailleurs ce que nous allons faire.

Après ça, il nous suffira d'écrire ...

```vala
string traduction = traduire ("Hello Clem !");
```

... pour que notre chaine `traduction` contienne le texte traduit.

Nous allons commencer par écrire une méthode `traduire` qui prendra en paramètre le texte qu'on veut traduire. Comme cette méthode va retourner quelque chose, il ne faut pas écrire `void` comme type de retour, mais `string` puisqu'on renvoie du texte.

```vala
string traduire (string texte_a_traduire) {

}
```

On va faire un traducteur très simple, qui va juste traduire *«Hello»* en *«Bonjour»*. Pour remplacer une portion de texte par une autre, il faudra utiliser `texte_a_traduire.replace ("texte à remplacer", "texte remplaçant");`.

```vala
string traduire (string texte_a_traduire) {
    string texte_traduit = texte_a_traduire.replace ("Hello", "Bonjour");
}
```

Enfin, pour renvoyer notre valeur, il suffit d'utiliser le mot clé `return` suivi de la valeur qu'on veut renvoyer.

```vala
return texte_traduit;
```

[[attention]]
| Le code après `return` ne sera pas exécuté.

Notre code de notre méthode `traduire` ressemble désormais à ceci.

```vala
string traduire (string texte_a_traduire) {
    string texte_traduit = texte_a_traduire.replace ("Hello", "Bonjour");
    return texte_traduit;
}
```

On pourra alors assigner la valeur que renvoie notre méthode dans une variable, comme on l'a vu plus tôt.

```vala
string traduction = traduire ("Hello Clem");
print ("%s\n", traduction);
```

Le code ci-dessus affichera `Bonjour Clem`.

[[attention]]
| La variable ne contiendra pas la méthode, mais bien ce qu'elle renvoie.