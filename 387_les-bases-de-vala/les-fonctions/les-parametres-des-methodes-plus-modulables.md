Pour le moment, notre méthode n'est pas très utile : on pourrait très bien écrire ce code dans notre point d'entrée, on aurait un fichier plus court. On va donc la rendre plus modulable avec les paramètres. Avec ce système, on va pouvoir passer des valeurs d'une méthode à une autre et faire varier très légèrement le résultat.

# Définir et utiliser ses paramètres

Vous vous doutez bien qu'on ne va pas balancer n'importe quoi comme paramètre. Si ma méthode veut exploiter le nom d'une personne, et qu'on lui envoie un nombre, il risque d'y avoir quelques problèmes ... 

Heureusement on peut préciser quel type de paramètre on veut, ainsi que le nom qu'ils auront dans la méthode. On fait cela de cette façon :

```vala
void ma_methode ([type] [nom]) {
    // ...
}
```

Ici, type sera le type du paramètre (`string`, `int`, `double`, etc) et nom, son nom (comment ça vous aviez compris ? :p ). Ensuite, on peut utiliser notre paramètre comme n'importe quelle autre variable, mais uniquement dans le bloc de la méthode bien entendu.

Si on reprends notre exemple du salut, on pourrait améliorer la méthode en demandant le nom de la personne à saluer (un `string` donc) :

```vala
void saluer (string personne) {
    print ("Salut %s !", personne);
}
```

Vous devez vous douter que si on appelle `saluer` comme avant, on va avoir une belle erreur étant donné qu'on a pas précisé le paramètre nom. On a alors deux solutions pour éviter cela :

1. Préciser `personne`.
2. Donner une valeur par défaut à `personne`, et donc le rendre optionnel.

Et comme on est fous, on va étudier les deux solutions !

## Préciser un paramètre

Pour choisir quelle valeur envoyer dans notre méthode, c'est extrêmement simple. Lorsqu'on l'appelle, il suffit de mettre cette valeur entre parenthèses. On peut aussi y mettre une variable, sa valeur sera alors envoyée[^ref]. On peut donc faire ceci :

```vala
saluer ("Clem");

// ou ...

string nom = "Clem";
saluer (nom);
```

Dans les deux cas le résultat sera le même (`Salut Clem !`). Vous pouvez essayer avec d'autres chaînes de caractères, mais si vous passez un `int` ou autre en paramètres ... Aïe !

## Les valeurs par défaut

On peut aussi définir une valeur par défaut pour un paramètre. De cette façon, on rend ce paramètre optionel et on reste compatible avec notre ancien code, tout en saluant une personne en particulier. On peut aussi continuer à préciser le nom de cette personne si on veut, la valeur par défaut n'est là que si rien n'est précisé. Pour dire quelle sera la valeur par défaut de notre paramètre, il suffit de lui assigner une valeur, avec une syntaxe similaire que pour n'importe quelle autre variable. On obtient donc ce code :

```vala
void saluer (string personne = "inconnu") {
```

Si on lance notre code, on obtient alors ...

```
Salut inconnu !
```

... à moins d'avoir précisé un autre nom lorsqu'on appelle la méthode.

# Plusieurs paramètres

Vous pouvez demander plusieurs paramètres, simplement en les écrivant les uns à la suite des autres, séparés par des virgules. Si on continue sur notre exemple, on pourrait rajouter l'âge de la personne, sous la forme d'un entier (`int`). La méthode aurait alors cette forme.

```vala
void saluer (string personne, int age) {
```

Désormais, il faudra aussi préciser `age` lorsque l'on appellera `saluer`.

```vala
saluer ("Clem", 42);
```

[^ref]: Mais pas la variable elle-même. Pour cela il faut utiliser le mot-clé `ref`, que nous étudierons dans une autre partie de ce tutoriel.
