Maintenant, voici quelques exercices pour voir si vous avez bien commpris. Et rapelez-vous que même si vous n'obtenez pas exactement le même code que nous, ce n'est pas grave, du moment qu'il fonctionne.

# N°1 : Pythagore

Vous devrez créer une fonction qui renvoie la longueur (en centimètres) de l'hypoténuse d'un triangle rectangle à partir de la longueur des deux autres côtés. Pour ceux qui ne se rappellent pas comment faire pour calculer cette longueur, il vous suffit de faire $\sqrt{côté1^{2} + côté2^{2}}$ . Il faudra ensuite afficher la valeur (depuis le `main`).

[[information]]
| Pour calculer la racine carrée d'un nombre il faut utiliser `Math.sqrt (nombre);`.

## Correction

[[secret]]
| ```vala
| // Le point d'entrée
| void main () {
|    // On calcule l'hypothénuse puis on l'affiche.
|    double hypotenuse = pythagore (5.7, 4.8);
|    print ("L'hypotenuse mesure : %lf cm.", hypotenuse);
| }
| 
| // La méthode pour calculer l'hypothénuse
| // Elle prend comme paramètres la longueur des deux autres côtés du triangle
| double pythagore (double cote1, double cote2) {
|     // On met les longueurs au carré
|     double carre1 = cote1 * cote1;
|     double carre2 = cote2 * cote2;
|     // On renvoie la racine carrée des deux carrés des longueurs
|     return Math.sqrt (carre1 + carre2);
| }
| ```

------------

# N°2 : Fibonacci

Calculer le n^ième^ nombre de la suite de Fibonacci est un exercice très courant en programmation, et il n'est pas réalisable sans les méthodes[^elegant].

Pour calculer un nombre dans cette suite, il suffit d'additionner les deux précédents. On considère aussi que pour 1, le résultat est 1. Ainsi, les 10 premiers nombres de cette suite sont 1, 1, 2, 3, 5, 8, 13, 21, 34 et 55.

Votre but sera de faire une méthode, qui prends en argument un nombre *n*, et de calculer le *n*^ième^ nombre dans la suite de Fibonacci et le retourner. Petit indice, vous allez devoir utiliser ce qu'on appelle de la **récursivité** c'est-à-dire une méthode qui s'appelle elle-même.

## Correction

[[secret]]
| ```vala
| void main () {
|     // On prends le 10ème nombre, mais on peut changer cette valeur.
|     int n = 10;
|     // On calcule le nombre et on l'affiche.
|     print ("Le %dème nombre de la suite de Fibonacci est : %d !\n", n, fib (n));
| }
| 
| // C'est ici qu'on calcule le nombre
| int fib (int n) {
|     if (n == 1) {
|         return 1; // Si on veut le premier, on renvoie 1.
|     } else {
|         return fib (n - 1) + fib (n - 2); // Sinon on renvoie la somme des deux précédents
|     }
| }
| ```

N'hésitez pas à inventer vos propre exercices, par exemple faire une fonction qui calcule la moyenne de deux nombres. ;)

[^elegant]: En fait, c'est possible, mais pas beau à voir (les codes moches, ça existe).