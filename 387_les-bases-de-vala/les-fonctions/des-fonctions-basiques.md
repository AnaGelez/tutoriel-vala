# Une fonction, qu'est ce que c'est ?

Une fonction est un morceau de code que vous pourrez exécuter à tout moment de votre programme (ou presque, il y a quelques limitations que nous verrons dans la seconde partie de ce tutoriel). Ce système nous évite donc de répéter les copier/coller et de grouper tout notre code au même endroit.

Prenons un exemple plus concret. Imaginons que vous souhaitiez envoyer un Tweet tout les jours à 22 heures grâce à un programme, mais que vous pouvez aussi décider de l'envoyer plus tôt en cliquant sur un bouton. Plutôt que d'écrire deux fois le code pour envoyer votre message, vous pourriez avoir une fonction qui s'en charge, et qui pourra être **appellé** (utilisée) en une seule ligne.

Mais les fonctions peuvent aussi changer légèrement, grâce aux **paramètres**. De cette façon vous pourriez au préalable changer le contenu de votre Tweet.

Enfin, les fonctions peuvent **retourner** des valeurs, que l'on pourra assigner à des variables. On pourrait ainsi savoir si l'envoi de notre Tweet a réussi.

Bref, ces sortes de programmes dans le programme sont très utiles. Et bonne nouvelle, nous allons tout de suite voir comment les utiliser !

# Définir et utiliser une fonction simple

Pour définir une fonction en Vala on utilisera toujours la syntaxe :

```vala
type nom (parametre_1, parametre_2, ..., parametre_n) { 
    // Instructions
}
```

Le type de retour de la fonction, est tout simplement l'un des types que l'on connaît : `string`, `double`, `int`, etc. Mais, si on ne souhaite pas que notre fonction renvoie une valeur on peut lui faire retourner le type `void`.

Le nom de la fonction suit les mêmes conventions que pour ceux des variables. Il sert à identifier la fonction, pour pouvoir l'appeler ensuite. Vous ne pouvez donc pas avoir deux fonctions qui ont le même nom dans un même programme.

Les paramètres sont des valeurs que l'on va donner lors de l'appel de notre fonction. Il pourront ensuite être utilisés comme n'importe quelle autre variable à l'intérieur du bloc de la fonction. Ça permet de rendre nos fonctions un peu plus modulables. Mais nous verrons comment les utiliser plus tard. Pour le moment on va laisser les parenthèses vides.

Ensuite, viennent les instructions de la fonction, qui seront exécutées lorsqu'on l'appellera.

Ça n'est peut être pas très clair, alors on va voir un exemple plus concret. On ne va pas prendre l'exemple précédent du Tweet, c'est un peu trop compliqué, bien que possible. (il faudrait savoir se connecter à Internet, envoyer une requête sur Twitter et avoir une autorisation). On va imaginer qu'on veut juste saluer l'utilisateur. Pour le moment, notre fonction ne renverra rien (on verra comment améliorer ça après). Son type de retour sera alors le type `void` (le mot pour *vide* en anglais). On pourrait lui donner un nom comme `saluer`, puisque c'est ce qu'elle va faire. Pour commencer elle ne prendra pas de paramètres : les parenthèses seront vides. On obtient donc ce code :

```vala
void saluer () {
    // ...
}
```

[[information]]
| En Vala, par convention, on laisse toujours un espace entre le nom de la fonction et les parenthèses qui le suivent.

[[question]]
| Attends, ça me fait penser à ... 
| 
| ```vala
| void main () {
|     // ...
| }
| ```
| 
| Est-ce que ça aussi c'est une fonction ?

Bien sûr ! Mais comme on l'a dit au début de ce tutoriel, la fonction `main` est un peu particulière : c'est la première que l'ordinateur va chercher à exécuter, il ne commencera jamais par `saluer` par exemple. On dit que c'est le **point d'entrée**. C'est donc à partir de cette fonction que nous en appellerons d'autres, qui pourront elles même en appeler d'autre et ainsi de suite.

Mais revenons à nos ~~moutons~~ fonctions. Il va maintenant falloir exécuter des actions dans notre nouvelle fonction. Là, rien de bien nouveau, on va simplement faire un `print`.

```vala
void saluer () {
    print ("Salut depuis une autre fonction !\n");
}
```

Vous venez d'écrire votre première fonction[^main] ! Bravo ! Et si on l'utilisait ?

# Appeler une fonction

Appeler une fonction, c'est exécuter le code qu'elle contient. Ce qui est très pratique avec le système des fonctions, c'est qu'on peut appeler des fonctions depuis n'importe quelle autre fonction. On peut même avoir un fonction qui s'apele elle-même[^rec] !

Et devinez quoi ? Vous avez déjà appelé une fonction : `print`. Au final, ce chapitre ne vous aura pas fait découvrir grand chose de nouveau, il vous l'aura juste fait comprendre. :p

Vous avez donc vu qu'il ce n'était pas très compliqué d'appeler une fonction. Il suffit de faire :

```vala
nom_de_ma_methode (arguments, si, il, y, en, a);
```

Donc, pour appeler la fonction `saluer` que nous venons d'écrire, il suffirait de rajouter ceci dans notre fonction `main`.

```vala
saluer ();
```

Si on ne met rien entre parenthèse, c'est tout simplement parce que notre fonction n'a pas encore d'arguments. Vous pouvez maintenant compiler votre programme. Vous devriez alors voir apparaître un petit message à l'écran.

```
Salut depuis une autre fonction !
```

Bravo vous savez maintenant créer des fonctions ! :D

[^rec]: On parle alors de fonction récursive.

[^main]: En réalité, vous aviez déjà écrit la fonction `main`.