Nous allons maintenant étudier ce qu'on appelle des **fonctions** (oui, comme les fonctions mathématiques :p ).

Le concept de fonction est présent dans la plupart des langages de programmation (certains comme [Haskell](https://zestedesavoir.com/tutoriels/674/apprenez-la-programmation-fonctionnelle-avec-haskell/) sont même entièrement basés sur ce concept). Ce sont des bouts de code réutilisables à n'importe quel moment. On évite donc de se répéter.

Mais les fonctions vont bien plus loin que ça : elles peuvent varier selon des paramètres et prendre différentes valeurs en fonction[^lol] de ceux-ci.

[^lol]: C'est le cas de le dire…