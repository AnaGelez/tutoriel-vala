Pour commencer dans Vala, l'écriture d'un programme de base, appelé *Hello, world !* s'avère nécéssaire. Il va nous permettre, de part sa fonctionnalité simplifiée au maximum (il se contente d'afficher "Hello, world !"), de comprendre comment est construit un programme en Vala.

# Fenêtre ou console ?

Pour commencer à programmer en Vala, nous n'allons pas faire de programmes fenêtrés (qui utilisent une interface graphique avec des boutons), mais de bons vieux programmes en console. Il faut savoir qu'en Vala, comme dans beaucoup d'autres langages de programmation généralistes, créer un programme utilisant une interface graphique est bien plus complexe que de créer un programme en console. C'est pour cela que nous allons commencer par des programmes en console, ils nous permettront d'aborder le langage progressivement.

Nous commenceront à voir les programmes en interface graphique plus tard dans le cours, pour l'instant notre programme ressemblera à ça :

-> ![Capture d'écran d'un Hello World en Vala.](/media/galleries/487/4c6e6b1c-b153-437b-b2bf-ac0ea466a71e.png.960x960_q85.png) 

 *Le programme est juste le "Hello world!", pas la fenêtre autour.* <-

# Structure du programme

Pour fonctionner, un programme Vala doit présenter une structure de base. Et même si vous n'avez pas encore programmé, ce programme vous semblera familier, c'est celui que nous avons utilisé pour tester le fonctionnement de Vala dans le chapitre précédent ! Le voici de nouveau pour vous rafraîchir la mémoire :

```vala
void main () {
    print ("Hello world !\n");
}
```

Ce code, très simple, est le code d'un programme *Hello World* de base (des versions plus évoluées existent), affichant une ligne de texte et terminant immédiatement. Maintenant, nous allons décortiquer un peu ce code et nous allons vous expliquer à quoi sert chaque partie de celui-ci.