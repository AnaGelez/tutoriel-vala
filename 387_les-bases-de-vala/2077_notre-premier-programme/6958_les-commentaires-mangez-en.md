Avant de passer à la suite, il est important de faire un point sur un sujet qui peu sembler anodin, mais quuui ne l'est pas forcément : les **commentaires**. Tout d'abord, posons nous la question suivante.

[[question]]
| C'est quoi un commentaire ?

Eh bien tout simplement, un commentaire, en Vala et *a fortiori* en programmation de manière générale, est une annotation que l'on met dans un programme pour indiquer quelque chose. Cette annotation sera tout simplement ignorée par le compilateur lorsqu'il construira le programme. Il n'y a donc aucune règle particulière à suivre à l'intérieur d'un commentaire pour qu'il soit valide.

La première utilité des commentaires, la plus évidente, est de décrire ce que fait le code qui compose votre programme. Pour le premier programme que nous avons fait, cela peut paraître inutile de rajouter des commentaires, mais pour des programmes plus compliqués, composés de plusieurs milliers de lignes de code, les commentaires deviennent nécessaires pour annoter certaines parties du codes non-évidentes ou encore pour décrire le fonctionnement de votre programme. De même, avec l'aide de certains outils, les commentaires peuvent vous aider à écrire la documentation technique de votre programme (on verra cela plus tard).

Une autre utilité des commentaires peut être pour le débogage (la correction des bugs) de votre application, afin de retirer un morceau de code du programme sans pour autant l'effacer du fichier. Il suffit de le commenter, et le compilateur l'ignorera. Cette utilisation ne vous est peut-être pas très parlante pour le moment (surtout si vous n'avez jamais programmé avant), mais vous en verrez rapidement l'utilité.

En Vala, comme dans beaucoup d'autres langages, deux types de commentaires existent, avec des symboles adaptés pour les définir :

- Les les blocs de commentaires
- Les commentaires sur une seule ligne.

## Les les blocs de commentaires

Les blocs de commentaires sont adaptés à l'écriture de plusieurs lignes de commentaires. En effet, ceux-ci prennent un symbole différent pour marquer le début du commentaire et pour le terminer. En Vala, ces symboles sont "`/*`" (ouverture de commentaire) et "`*/`" (fermeture de commentaire). Dans ce type de commentaire, le compilateur ignorera absolument *tout* ce qui est écrit entre ces deux ensembles de caractères. On utilise par exemple ce genre de commentaire en début de fichier pour indiquer certaines infos à propos du fichier de code écrit :

```vala
/* Fichier: hello.vala
   Auteur: John Doe
   Date de création: 10/09/2016
   Licence: GNU GPL
*/
```

[[information]]
| Ces commentaires peuvent également êtres utilisés sur une seule ligne, sous la forme `/* Ceci est un commentaire */`, et peuvent même être également utilisés en plein milieu d'une ligne de code, même si ce n'est pas très lisible.

## Les commentaires sur une seule ligne

Les commentaires sur une ligne, contrairement aux commentaires en blocs, ne peuvent prendre qu'une seule ligne, et par conséquent, n'ont qu'un seul caractère pour délimiter le début du commentaire, et s'arrêtent irrémédiablement à la fin de la ligne. Cela veut également dire que sur la même ligne, vous ne pouvez **pas** mettre de code après un commentaire de ce genre. En Vala, ce délimiteur est "`//`". Par exemple, une utilisation de ce type de commentaire est d'expliquer une ligne unique de code :

```csharp
// Affiche "Hello World" sur la console
print ("Hello, world !\n");

print ("Ça va ?\n"); // On peut également les mettre en fin de ligne
``` 

Bref, les commentaires, c'est utile pour que vous ne vous perdiez pas dans votre code. Enfin, n'oubliez jamais de commenter une partie de code qui semble un peu obscure, car, même si au moment où vous l'écrivez vous pensez connaître sa signification, il se peut que vous reveniez quelques mois plus tard sur ce code, que vous l'ayez oublié, et que vous perdiez donc du temps à essayer de comprendre de nouveau sa signification. De même, lorsque vous travaillez à plusieurs, il est impératif de commenter son code, car si une partie peut paraître logique pour vous, elle ne l'est pas forcément aussi facilement pour les personnes qui vont relire votre code. Pour terminer sur les commentaires, voici une petite citation (pouvant s'appliquer aux commentaires, et à leur utilisation) :

> Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live.
>
> *Codez toujours comme si la personne qui va maintenir votre code sera un violent psychopathe qui sait où vous habitez.*
Source: John F. Woods