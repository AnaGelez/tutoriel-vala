Voici les points les plus importants à retenir de ce chapitre :

- Nous écrirons d'abord des programmes en console, car ils sont plus simples à mettre en œuvre.
- La fonction `main` est le point d'entrée de notre programme. Tout le code que nous écrirons ira dedans (pour le moment).
- Les commentaires sont très importants pour rendre un code compréhensible et maintenable. On peut en écrire en les encadrant de `/*` et de `*/` ou en les écrivant après un `//`.

Maintenant qu'on a vu un peu plus en détail comment fonctionnait Vala, on va passer à la programmation, la vraie ! :pirate: