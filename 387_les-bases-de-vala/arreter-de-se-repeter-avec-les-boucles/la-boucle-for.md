Vala nous propose un autre type de boucle, la boucle `for`. Elle s'écrit de cette façon.

```vala
for (instruction1; predicat; instruction2) {
    
}
```

On va décortiquer cette syntaxe un peu particulière :

- `instruction1` est une instruction qui s'exécutera juste avant le début de la boucle ;
- `predicat` est le prédicat pour sortir de la boucle (on en sort quand il vaut `false`) ;
- `instruction2` est une autre instruction, mais celle-ci est exécutée après chaque tour de la boucle.

On peut reprendre notre précédent exemple de la table de multiplication et le réécrire de cette façon avec la boucle `for`.

```vala
const int TABLE = 7;

for (int i = 0; i <= 10; i++) {
    print ("%d × %d = %d\n", TABLE, i, TABLE * i);
}
```

On peut aussi imbriquer une boucle dans une autre. Par exemple, on utilise ici deux boucles `for` pour afficher toutes les tables de multiplication de 1 à 10.

```vala
for (int i = 1; i <= 10; i++) {
    print ("\nTable de %d\n\n", i);
    
    for (int j = 0; j <= 10; j++) {
        print ("%d × %d = %d\n", i, j, i * j);
    }
}
```

[[information]]
| Deux boucles imbriquées n'ont pas forcément besoin d'être les mêmes : on peut très bien mettre une boucle `for` dans une boucle `while`.