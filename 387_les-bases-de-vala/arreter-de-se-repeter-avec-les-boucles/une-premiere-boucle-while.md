La première boucle que nous allons étudier est la boucle `while` (« tant que » en Anglais). Comme son nom l'indique elle va nous permettre de répéter des instructions *tant que* quelque chose est vrai. Quelque chose comme un booléen… :p

On l'utilise avec cette syntaxe.

```vala
while (predicat) {
    
}
```

Et on placera les instructions à répéter dans le bloc. Ici, `predicat` est un booléen qui sera vérifié à chaque tour de la boucle, jusqu'à ce qu'il devienne `false`, et donc que la boucle s'arrête. Un exemple très simple nous permet par exemple d'afficher une table de multiplication.

```vala
const int TABLE = 4;
int i = 0;

print ("Table de %d :\n", TABLE);
while (i <= 10) {
    print ("  %d × %d = %d\n", TABLE, i, TABLE * i);
}
```

Vous remarquerez d'ailleurs que j'ai nommé une variable `i`, alors que nous vous l'avions déconseillé il y a quelques chapitres ! En fait, par convention on utilise de simples lettres pour les variables servant uniquement pour le prédicat des boucles, et on choisit souvent `i` (puis on continue dans l'ordre de l'alphabet si besoin).

# La boucle do/while

Il existe une autre boucle qui ressemble un peu à `while` : `do while`. La seule différence avec `while` est qu'elle va exécuter les instructions au moins une fois, même si le prédicat est faux, car celui-ci est vérifié à la fin de chaque tour. On l'écrit de cette façon.

```vala
do {

} while (predicat);
```

Voici un petit exemple.

```vala
do {
    print ("Salut !\n");
} while (false);
```

Ici, même si le prédicat vaut clairement `false`, on verra le message s'afficher.

Cette boucle est un peu moins souvent utilisée que la boucle `while`, mais souvenez-vous en quand même. ;)

# Les boucles infinies : vos pires ennemies

Regardez le code suivant, et essayez d'imaginer ce qu'il va faire.

```vala
int i = 0;
while (i < 10) {
    print ("Bonjour !\n");
}
```

Avec ce prédicat, on ne sortira jamais de la boucle, car `i` n'est pas modifié pendant la boucle et il restera donc toujours inférieur à dix. Notre programme va donc afficher son petit message sans jamais s'arrêter. C'est assez problématique, mais ce genre de boucle qu'on appelle les **boucles infinies**, peuvent être bien plus grave. Imaginez que l'on écrive dans un fichier à chaque tour de boucle par exemple : notre disque dur risque de souffrir si on ne s'arrête jamais.

[[information]]
| Si jamais vous exécutez un de vos programme et que vous vous rendez compte que vous avez créé une boucle infinie, vous pouvez l'arrêter en appuyant en même temps sur les touches ||Ctrl|| (ou ||⌘|| sur Mac OS X) et ||C||.

Il faudra donc bien faire attention a ne pas créer de boucles infinies dans votre programme. La plupart du temps c'est juste qu'on oublie de modifier une variable. ;)