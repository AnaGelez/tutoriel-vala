En résumé :

- pour écrire des boucles en Vala on utilise les mot-clés `while`, `do`/`while` et `for` ;
- il faut faire attention à ne pas créer de boucles infinies ;
- on peut utiliser `break` et `continue` pour contrôler le déroulement de nos boucles.

Dans le prochain chapitre nous allons découvrir les tableaux.