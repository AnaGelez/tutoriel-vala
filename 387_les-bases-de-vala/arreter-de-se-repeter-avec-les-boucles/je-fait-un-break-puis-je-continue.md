Vala nous permet aussi de contrôler l’exécution de nos boucles, en passant au tour suivant ou en arrêtant carrément la boucle avant l'heure. On va pour cela utiliser deux nouveaux mot-clés : `continue` et `break`.[^nouveau]

# Le mot-clé continue

`continue` est une instruction à lui seul. En le plaçant à un endroit de notre boucle, on va indiquer à notre programme de passer au tour suivant, sans finir l'actuel. On ne va donc pas le placer directement dans le bloc de notre boucle, mais en général dans une condition elle-même dans la boucle.

```vala
const int MAX = 100;
for (int i = 0; i < MAX; i++) {
    if (i % 2 == 0) {
        continue;
    }
    print ("%d est impair.\n", i);
}
```

Ici, si le modulo (le reste de la division euclidienne) de `i` est 0 (c'est-à-dire si `i` est pair), on passe au tour suivant de la boucle. Quand `i` est impair, la boucle s'exécute jusqu'au bout et on voit le message.

# Sortir de la boucle

On peut aussi complètement sortir d'une boucle et passer à la suite de notre programme avec l'instruction `break`.

```vala
print ("Entrez deux nombres :\n");
int a = int.parse (stdin.readline ());
int b = int.parse (stdin.readline ());

int min = (a < b) ? a : b; // On récupère le plus petit nombre

for (int i = 2; i <= min; i++) { // pour chaque nombre allant de 2 à min (1 ne nous intéresse pas, il divise tout)
    if (a % i == 0 && b % i == 0) { // on regarde si il divise à la fois a et b
        print ("Le plus petit diviseur de %d et %d est %d.\n", a, b, i);
        break;
    }
}
```

Ce programme détermine le plus petit diviseur commun de deux nombres à l'aide d'une boucle et sort de celle-ci une fois qu'il a été trouvé. Sans l'instruction `break` on aurait la liste de tous les diviseurs communs de `a` et `b`.

[^nouveau]: Bon en fait on connais déjà `break` puisqu'on l'utilisait aussi dans les `switch`s.