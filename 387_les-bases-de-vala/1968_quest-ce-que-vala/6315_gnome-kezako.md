Avant de commencer à parler de Vala, une petite introduction sur [GNOME](http://www.gnome.org) s'avère nécéssaire.

GNOME, c'est un environnement de bureau[^de] sous Linux, un des plus anciens avec son alternative [KDE](http://www.kde.org). A l'opposé de celui-ci, développé à l'aide du framework Qt, GNOME est basé sur Gtk+, un ensemble de bibliothèques[^bibli] développés autour de la bibliothèque graphique du même nom. De même, à l'instar de KDE qui contribue au développement de Qt, GNOME contribue très fortement au développement de tout Gtk+ (à la base créé pour le logiciel de retouche d'image GIMP), et en assure même la maintenance principale depuis plusieurs années. Les développeurs de GNOME ont eu ainsi tout le loisir de développer le framework[^fw] pour proposer le meilleur environnement de bureau possible.

-> ![Affichage des activités de GNOME 3.12](https://zestedesavoir.com/media/galleries/487/a98fe412-3248-4567-9d47-ac2ac09358ec.png.960x960_q85.png) <-

Le projet est activement développé et intègre des technologies modernes. Par exemple, avec l'avènement récent de GNOME 3 et par la même occasion de Gtk+ 3.0, le projet a désiré rendre son environnement graphique plus adapté aux interfaces tactiles (tablettes et PC tactiles), et par conséquent les interfaces développées avec cette nouvelle version disposent de fonctionnalités adaptées à de telles interfaces (par exemple la gestion des *touch gestures*). En développant pour GNOME 3, vous assurerez donc la compatibilité de vos applications avec ces nouvelles interfaces.

[^de]: **Un environnement de bureau** est un programme qui gère l'interface du système : le style des fenêtres, l'emplacement des raccourcis, de la barre de notifications, etc.

[^bibli]: **Une bibliothèque** (en informatique) est un ensemble de fonctions qui peuvent être réutilisés par n'importe quels programmes. Par exemple, on peut citer la bibliothèque GStreamer qui permet de manipuler des fichiers audio et vidéo facilement. Ainsi tous les programmes qui ont besoin de jouer une musique, de modifier une vidéo, etc, peuvent réutiliser GStreamer sans avoir à gérer cet aspect qui peut s'avérer très complexe.

[^fw]: **Un framework** est une très grosse bibliothèque (ou un ensemble de bibliothèques) qui permet de développer des applications complètes presque à lui seul.