# Pourquoi un nouveau langage ?

Comme dit précédemment, l'environnement GNOME se base sur le framework Gtk+. Les bibliothèques le composant sont nombreuses : GIO (pour les opérations d'entrée/sortie), GVFS (système de fichiers virtuel), Gee (pour gérer des structures de données complexes)... Toutes ces bibliothèques assurent leur compatibilité via la base du framework, la bibliothèque GLib.

Ces bibliothèques sont toutes écrites en C, mais il existe de nombreux ports de ces bibliothèques vers d'autres langages (plus ou moins complètement, la plupart des langages intégrant juste Gtk+) : C++ (appelée GtkMM, bien qu'il soit toujours possible d'utiliser la version originelle de GLib en C++), C#, JavaScript, PHP, Python, Ruby... Mais malgré ce grand nombre de portages, encore beaucoup d'applications Gtk+/GLib sont développées en C.

De cette situation est née en 2006 un nouveau langage : **[Vala](http://wiki.gnome.org/Projects/Vala)**. Ce langage, créé par les mainteneurs de Gtk+, est destiné à fournir aux développeurs un langage **moderne**, **complètement intégré** dans l'écosystème Gtk+, et fournissant une **interface commune** avec les programmes déjà écrits en C. De plus il facilite le développement d'applications par rapport au C, qui est dit *bas-niveau*, c'est-à-dire plus proche du système mais aussi plus compliqué à manipuler.

[[information]]
| Comme nous l'avons dit dans le paragraphe précédent, Vala date de *2006*. C'est un langage encore jeune, il est sujet à des mises à jour fréquentes. Si vous souhaitez programmer en Vala, nous vous conseillons de surveiller les mises à jour du langage.

# Qu'est-ce que Vala précisément ?

Bon, pour l'instant, on a parlé de GNOME, de Gtk+, mais toujours pas de Vala en soi.

Vala et un langage de programmation. Ça ne vous dit rien ? Alors, nous vous conseillons de commencer par lire [ce petit tutoriel](https://zestedesavoir.com/tutoriels/531/les-bases-de-la-programmation/) qui explique ça très bien, puis de revenir ici.

Vala est un langage **orienté objet**, c'est à dire que tout dans le langage est défini par ce qu'on appelle des **objets** (et également des **classes**, contrairement à JavaScript, dont le modèle objet n'utilise pas de classes). Mais nous étudierons cela plus en détail en temps voulu, nous utiliserons d'abord Vala en programmation impérative (tel que l'on programme en C par exemple).

Sa syntaxe se rapproche de celle du C#, le langage phare de Microsoft pour sa plateforme .NET. Par contre, à l'opposé de celui-ci, Vala n'est pas **interprété** par une machine virtuelle, mais **compilé en langage machine**.

[[information]]
| Pour rappel, le langage machine est le format utilisé dans les binaires de vos programmes, lu directement par le processeur et contenant les instructions qu'il doit suivre pour exécuter vos programmes Ce format n'est normalement pas lisible par les humains (sauf par des humains très expérimentés, au prix de longues heures).

Ce mode de fonctionnement apporte plusieurs différences avec des langages comme C#, Java, Python ou Ruby, des langages dits *interprétés* :

* Les programmes compilés en langage machine tournent généralement plus vite que les programmes interprétés (bien qu'avec les technologies dont font preuve les nouveaux langages interprétés, cette différence est de plus en plus ténue).
* Les programmes compilés en langage machine n'ont pas besoin d'avoir d'interpréteur installé pour fonctionner.
* Souvent, les programmes compilés gèrent moins automatiquement la mémoire que les programmes interprétés et il faut donc sans cesse la gérer à la main.
* Les programmes interprétés sont portables (ils peuvent être exécutés sur plusieurs systèmes d'exploitation sans avoir à être modifiés) alors que les programmes compilés doivent être recompilés pour chaque système, et même modifiés pour être compatibles.

Néanmoins, Vala arrive à contrecarrer certains désavantages des langages évoqués dans cette liste grâce à son mode de compilation un peu différent des autres. En effet, contrairement au C++ qui est directement compilé en binaire par exemple, le Vala est d'abord compilé en **C** (*via* le compilateur Vala), qui lui-même est compilé en langage machine par GCC (ou n'importe quel autre compilateur C, mais GCC est préféré).

Cette étape supplémentaire de compilation apporte plusieurs avantages à Vala :

* Vala peut interagir très facilement avec n'importe quelle bibliothèque écrite en C, ou même n'importe quel code écrit en C.
* N'importe quelle bibliothèque C peut être portée en Vala avec juste un fichier décrivant la structure des fonctions de la bibliothèque en Vala.
* Le compilateur de Vala est suffisamment intelligent pour générer un code différent selon le système sur lequel on compile, pour être sur que notre programme fonctionne partout. Ainsi, des programmes écrits en Vala peuvent être recompilés sans aucune modification et fonctionner sur un autre système.

# Quelques exemples de programmes écrits en Vala

Malgré sa jeunesse, plusieurs programmes plus ou moins connus ont quand même été écrits en Vala. Certes vous ne trouverez pas dans ces exemples le dernier Call of Duty ou le dernier logiciel multimédia à la mode, néanmoins ces projets permettent de prouver que Vala est un langage viable pour le développement. La plupart (pour ne pas dire tous) des logiciels présentés ici sont spécifiques à Linux :

* [Unity](http://unity.ubuntu.com), le gestionnaire de bureau d'Ubuntu, est écrit partiellement en Vala.	
* Plusieurs applications par défaut de GNOME (Baobab, Cheese, Lights Off, Mahjongg, Démineur...) sont programmées en Vala.
* Vala a été désigné comme langage de choix pour [Elementary OS](http://elementaryos.org), une distribution Linux dérivant d'Ubuntu. Par conséquent, plusieurs de ses logiciels par défaut (Geary, un client mail, Scratch, un éditeur de texte temps-réel...) sont développés en Vala.
* Shotwell, un visionneur de photos puissant, est développé en Vala.
* Pino, un client de microblogging (Twitter, Identi.ca) est également programmé en Vala.

Pour plus d'exemple, il vous suffit de voir la [liste des projets](https://wiki.gnome.org/Projects/Vala/Documentation#Projects_Developed_in_Vala) (plus ou moins importants) réalisés en Vala
