Maintenant que j'ai assez parlé de Vala pour vous mettre l'eau à la bouche, on va pouvoir l'installer et l'essayer.

Au niveau du système d'exploitation pour développer en Vala, nous vous conseille d'utiliser une distribution GNU/Linux (même en machine virtuelle), les outils disponibles sont bien plus intéressants que sur les autres systèmes. Néanmoins, vous pouvez toujours utiliser votre OS préféré pour développer en Vala, simplement moins d'outils vous seront disponibles.

Au niveau de l'environnement de programmation, Vala n'impose rien. Au début, Vala possédait un environnement de développement « officiel » développé lui-même en Vala, Val(a)IDE, mais ce projet est abandonné depuis longtemps, nous ne vous conseillons donc pas de l'utiliser, d'autres éditeurs seront bien mieux.
Plusieurs éditeurs proposent le support de Vala, de manière plus ou moins complète, mais nous vous conseillons un des trois suivants :

* [Geany](http://www.geany.org), un IDE léger et puissant, capable de fonctionner sous Linux, Mac (via MacPorts) et Windows. Il permet l'auto-complétion, l'intégration avec le compilateur (et plus ou moins avec le débogueur, via un plugin), une gestion basique de projets. C'est une bonne alternative à Anjuta, et un bon éditeur de texte pour coder dans d'autres langages.
* [Anjuta](http://anjuta.org), l'IDE développé par GNOME (on y revient toujours), propose un support de Vala poussé, une auto-génération des projets avec scripts configure/Makefiles, un concepteur d'interface graphique, et plein d'autres choses. C'est le meilleur IDE pour Vala. Seul hic, il n'est disponible que sous Linux.		
* [Sublime text](http://www.sublimetext.com/3), qui sert juste en tant qu'éditeur de texte, mais qui propose la coloration syntaxique de code Vala via un plugin disponible sur *Package Control*. Une recherche Google devrait vous aider à trouver ce qu'il vous faut.

Bien sûr, vous n'êtes pas obligés d'utiliser l'un de ces éditeurs et utiliser le vôtre (même le bloc-notes Windows), par contre vous ne pourrez peut-être pas profiter des avantages que possèdent les éditeurs supportant Vala (comme la coloration syntaxique, la table des symboles...). 

# Installation sous Linux

Sous Linux, la plupart des distributions possèdent dans leurs dépôts une version de Vala, l'installation devient alors triviale. Il vous suffit juste d'installer le compilateur Vala, `valac`, avec votre gestionnaire de paquet préféré (il installera normalement les dépendances nécessaires tout seul).

Pour Debian et dérivés par exemple, il suffit d'exécuter cette commande :

```bash
sudo apt-get install valac
```

[[attention]]
| Le rythme des sorties des nouvelles versions de Vala étant assez rapide, certaines distributions (notamment celles voulant rester stable, comme Debian) ne se mettent pas à jour directement. Pour ces cas-là, une compilation de la dernière version de Vala peut se trouver préférable (notamment pour travailler sur de gros projets). Néanmoins, pour ce tutoriel, ce ne sera pas nécessaire.

C'est tout pour l'installation de Vala sous Linux.

# Installation sous BSD

Pour installer Vala sous BSD, vous pouvez simplement utiliser la commande suivante.

```bash
pkg install vala
```

Et voilà, c'est bon. :)

# Installation sous Mac OS X

Sous Mac OS X, Vala n'est pas disponible sous la forme d'une image DMG, pour l'installer il vous faudra passer par l'utilitaire MacPorts. Pour le télécharger, il suffit de se rendre sur le site officiel de l'utilitaire.

-> **[Site officiel de MacPorts](http://www.macports.org)** <-

Une fois sur le site officiel, cliquez sur le lien ["Installing MacPorts"](http://www.macports.org/install.php), puis dans la section "Mac OS X package (.pkg) Installer", téléchargez la version qui correspond à votre système en cliquant sur le lien portant le nom de votre version de Mac OS X.

[[information]]
| Pour connaître la version de Mac OS X que vous utilisez, ouvrez le menu Apple dans la barre de menu, puis cliquez sur l'entrée "A propos de ce Mac...". La fenêtre s'ouvrant alors contient le numéro de version de votre Mac.

[[attention]]
| Pour pouvoir utiliser MacPorts sur votre système, il vous faudra d'abord avoir installé XCode. Vous pourrez le trouver sur votre disque d'installation de Mac OS X, ou directement sur l'App Store.

Quand l'installateur de MacPorts a fini de télécharger, ouvrez-le et suivez les instructions. Une fois que c'est terminé, il vous suffit pour installer Vala d'exécuter la commande :

```bash
sudo port install vala
```

Après avoir tapé votre mot de passe, MacPorts va télécharger Vala, le compiler et l'installer sur votre système. Vous n'avez rien d'autre à faire.

# Installation sous Windows

Sous Windows, GNOME ne fournit pas de binaires officiels pour Vala (en tout cas, pas de binaires à jour), tout comme les autres systèmes où ce sont les packageurs qui ont compilé Vala pour l'offrir (ou Mac OS où nous devons le compiler nous même). Néanmoins, Tarnyko, un membre de la communauté a compilé une version pour Windows, contenant tout ce dont nous auront besoin pour ce cours.

Tout d'abord, cliquez sur le lien suivant pour accéder au dépôt des installateurs Vala pour Windows :

-> **[Dépôt Vala de Tarnyko](http://www.tarnyko.net/dl/vala)** <-

Ensuite, téléchargez l'installateur pour la version la plus à jour de Vala (la version 0.20.1 à l'heure où j'écris). Une fois téléchargé, lancez-le, puis suivez les instructions de l'installateur (autrement dit, faites "Suivant" jusqu'à ce qu'il ait installé Vala). C'est tout.

# Vérification du fonctionnement de Vala

Maintenant que Vala est installé, nous allons vérifier qu'il marche bien, qu'aucune bibliothèque ne manque. Pour cela, créez un nouveau fichier dans votre éditeur de texte favori, puis collez-y ce code (il provient du premier exemple disponible sur [le site officiel](https://live.gnome.org/Vala/BasicSample)) :

```vala
void main () {
    print ("Hello world !\n");
}
```

Enregistrez ce fichier (chez nous, c'est `hello.vala`). Maintenant, il va falloir compiler et exécuter ce code. Pour cela il va falloir utiliser ... **la ligne de commande**. :pirate:

## La ligne de commande ?

La ligne de commande est une autre façon d'interagir avec votre ordinateur. Plutôt que de cliquer sur des boutons, on va taper des commandes, dans un **terminal**, comme les pirates informatiques dans les films (mais on ne fait rien d'illégal ne vous inquiétez pas ^^ ).

Pour ouvrir un terminal, tout dépend de votre système :

- Sous Mac OS X, il faut aller dans le dossier `Applications/Utilitaires` et exécuter l'application `terminal.app`.
- Sous Windows, vous pouvez rechercher « PowerShell » et prendre le premier résultat.
- Sous Linux, tout dépends de votre distribution, mais vous devriez pouvoir le trouver facilement dans la liste des applications installées.

Maintenant que vous avez un terminal ouvert, il faut apprendre à l'utiliser. Pour commencer, il faut savoir que vous avez un dossier courant dont vous pouvez manipuler les fichiers. Vous pouvez connaître le dossier dans lequel vous vous trouvez en entrant la commande `pwd`, puis en validant avec ||Entrée||.

Vous pouvez connaître tous les fichiers et dossier qui sont dans le dossier actuel, grâce à la commande `ls`.

Vous pouvez changer de dossier simplement en tapant la commande `cd`, suivie du nom du dossier, puis en la validant. Si vous faites `pwd` maintenant, vous ne devriez pas avoir le même résultat que tout à l'heure. Vous pouvez aussi aller dans le dossier parent au dossier actuel, en faisant `cd ..`. Essayez de vous déplacer jusqu'au dossier ou vous avez mis votre code Vala avec des `cd` (et n'hésitez pas à utiliser `pwd` et `ls` pour ne pas vous perdre ;) ).

Maintenant, si vous êtes bien dans le dossier où se trouve votre code, on va le compiler. Pour cela, entrez cette commande, puis validez.

```bash
valac hello.vala
```

Cela devrait créer un programme `hello` à côté du fichier `.vala` (`hello.exe` pour Windows). Pour lancer ce programme, il faut utiliser la commande `./hello`, ou `hello.exe` sous Windows. Vous devriez alors voir apparaître un "Hello, World !". Si ce n'est pas le cas, l'installation a dû rater à un endroit, nous vous conseillons de revoir la procédure que vous avez suivi (si vous n'arrivez vraiment pas à trouver, nous vous invitons à nous envoyer un message privé).

[[information]]
| Les utilisateurs de Geany (et également d'autres éditeurs compatibles, à vous de trouver lesquels) pourront compiler et lancer le programme sans ouvrir de console, juste en cliquant sur le bouton "Construire" dans la barre d'outils (il ressemble à une brique), puis sur le bouton "Exécuter" (une roue dentée). Sous Windows, pour exécuter, vous devez d'abord ouvrir le menu "Construire", puis aller dans "Définir les commandes de construction", pour remplacer le contenu de "Exécuter" par `%e.exe` puis valider (et enfin cliquer sur "Exécuter").
