Maintenant, nous allons opérer sur les nombres. Il faut savoir que votre ordinateur, même si il est capable de faire plein de trucs magiques comme afficher des graphismes 3D époustouflants, avoir des vidéoconférences à travers le monde ou bien créer de supers morceaux de musique, ne reste qu'une grosse calculatrice. Exécuter des opérations sur des nombres est donc une chose qu'il fait parfaitement bien, et de manière *très rapide*.

Tout d'abord, les 4 opérations de base : **addition, soustraction, multiplication et division**. Pour effectuer celles-ci, rien de plus naturel : Vous tapez votre opération avec les *opérateurs* classiques (+, -, \*, /) et leurs *opérandes* (les nombres ou variables composant le calcul), comme vous feriez dans une calculatrice :

```vala
int a, b, c;

a = 2;
b = 5;

c = a + b; // 2 + 5 = 7
print ("%d + %d = %d\n", a, b, c);

c = b / a; // 5 / 2 = 2.5 ?
print ("%d / %d = %d\n", b, a, c);
```

Si vous exécutez ce dernier exemple, vous verrez que le résultat de la division n'est pas celui que l'on attend. En effet, au lieu de nous afficher "5 / 2 = 2.5", le programme nous affiche "5 / 2 = 2". L'ordinateur se serait-il trompé ?

Non, il ne s'est pas trompé. Le problème est que la variable où l'on souhaite stocker le résultat de notre division est de type `int`, et comme dit plus haut, les types `int` **ne peuvent stocker que des nombres entiers**. Et comme notre ordinateur écoute bêtement ce qu'on lui dit, même si le résultat est un nombre décimal, il ne va pas mettre de partie décimale là où il ne peut pas ! Il va donc uniquement conserver **sa partie entière**. Pour avoir le résultat de notre division avec la partie décimale, il faut alors qu'au moins une partie de l'opération soit de type `float` ou `double` :

```vala
float a, b;

a = 2;
b = 5 / a;

print ("5 / %f = %f", a, b); // Affiche le bon résultat, avec plein de zéros derrière
print ("5 / %g = %g", a, b); // Affiche le bon résultat, sans les zéros
```

Ensuite, vient une autre opération de base, que nous n'avons pas mentionné plus tôt parce que vous ne la connaissez probablement pas. Le **modulo**. Derrière ce nom cache une opération bien simple : le modulo est tout simplement le reste d'une *division euclidienne* (si vous ne vous en souvenez pas, une division euclidienne est une division où l'on s'arrête aux nombres entiers, sans faire de quotient à virgule. Il y a donc un reste). Par exemple, le modulo de 5 par 2 est **1**, car 5/2 = 2 avec un reste de 1. On note le modulo avec le signe `%`, ce qui donne par exemple :

```vala
int a, b, c;

a = 5;
b = 2;
c = a % b;

print ("%d %% %d = %d\n", a, b, c); // Affiche "5 % 2 = 1"
```
## Raccourcis

Avant de passer à la suite, il reste quelques astuces à savoir sur les opérations entre nombres. Il arrive assez souvent que le résultat d'une opération soit stocké dans une variable qui en est membre (autrement dit, un des opérandes accueille également le résultat). Il est donc souvent commun de tomber sur des opérations comme celle-ci :

```vala
int vie = 50;
int attaque = 20;

vie = vie - attaque;
```

Ce genre d'opération est assez embêtant à écrire, et **les développeurs sont de gros flemmards** (et aussi que ça prend un poil de mémoire en plus). Pour palier à cela, il existe un certain nombre de raccourcis.

Les premiers de ces raccourcis sont destinés à toutes les opérations arithmétiques, quelles qu'elles soient, du moment que l'une des opérandes est également la variable à laquelle le résultat sera assigné. Les symboles pour ces *assignements composés* sont `+=`, `-=`, `*=`, `/=` et `%=`. Ils fonctionnent donc comme une assignation `=` et une opération exécutées en même temps. Ainsi, l'exemple précédent devient :

```vala
int vie = 50;
int attaque = 20;

vie -= attaque; // vie vaut maintenant 30.
```

[[information]]
| Ces opérations fonctionnent comme n'importe quel opérateur, et donc renvoient une valeur. Vous pouvez donc assigner le résultat de votre à une autre variable en plus de la variable modifiée en utilisant `=` de la manière suivante : `foo = bar -= 5;` par exemple.

Dans ces calculs précédemment cités, il est important de savoir qu'il existe un certain calcul encore plus utilisé que les autres. Ce calcul est vraiment basique : ajouter ou retirer 1 à une variable (j'utilise le singulier car ceci n'est réellement qu'un seul calcul, la soustraction n'étant que l'addition d'un nombre négatif). Et ce calcul est utilisé tellement de fois, à tellement d'endroits différents, que l'on a décidé de lui donner son propre nom : **l'incrémentation** (pour l'ajout) et la **décrémentation** (pour le retrait). On en a même profité pour lui donner son propre opérateur raccourci, encore plus concis que les autres[^flemme] : `++` et `--`.

Contrairement aux autres opérateurs, ces opérateurs ne prennent pas 2 opérandes, mais une seule : la variable à laquelle on veut ajouter ou retirer 1. L'opérande peut se placer de n'importe quel côté de cet opérateur. Ces opérateurs sont appelés des **opérateurs unaires**. Il s'utilisent de la manière suivante :

```vala
int vies = 5;

vies--; // vies vaut maintenant 4
vies++; // vies vaut de nouveau 5

++vies; // vies vaut 6
--vies; // vies vaut 5;
```

A cette utilisation vient s'ajouter une petite particularité. Il faut savoir que, comme tous les autres opérateurs, celui-ci renvoie une valeur (que l'on peut utiliser dans un autre calcul ou assigner à une variable) en plus de modifier la variable. Néanmoins, il ne renvoie pas la même valeur si vous le mettez *avant* ou *après* l'opérande. S'il est mis avant, il renverra la valeur augmentée (ou diminuée) de 1. S'il est mis après, il renverra la valeur actuelle. Cela peut être utile pour récupérer la précédente valeur d'une variable que l'on va incrémenter.

[^flemme]: Moins ces flemmards de programmeurs ont a écrire, mieux c'est.