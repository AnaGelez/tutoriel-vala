Si vous avez dépassé le niveau 3^ème^ en mathématiques, vous avez déjà une définition pour une variable : 

> Une valeur arbitraire, pas totalement précisée, ou même inconnue - appartenant à un ensemble - sur laquelle est effectuée une combinaison d'opérations avec d'éventuelles constantes
Source: Wikipédia

En informatique, une variable ce n'est pas très différent. Une variable représente, de manière abstraite, une donnée de votre programme. Cette donnée doit être forcément définie pour être utilisée (à contrario des mathématiques, où une variable peut ne pas être reliée à une valeur concrète), et possède un type : nombre entier, nombre décimal, chaîne de caractères (du texte)... Notez que cette définition n'est pas réduite à Vala, mais est générale à plus ou moins tous les langages de programmation.

Cette variable a également un nom, qui sert à l'identifier pendant son utilisation. En Vala, ce nom est contraint d'utiliser uniquement les caractères alphanumériques majuscules et minuscules (de A à Z, et de 0 à 9), ainsi que le symbole *underscore* (le « _ », parfois appelé « tiret du 8 » en France), et ne peut pas commencer par un chiffre. Bien entendu, vous ne pouvez pas appeler deux variables différentes avec le même nom, sinon elles deviendraient impossible à différentier. Néanmoins, vous pouvez changer la valeur associée à une variable.

[[information]]
| Par convention, les variables en Vala ont des noms en *snake_case*. Ça veut dire qu'ils ne contiendront pas de majuscules, et qu'on utilisera l'*underscore* pour représenter les espaces si besoin. Par exemple, on ne nommera pas une variable `PointsDeVie`, mais `points_de_vie`.

# Les types

En Vala, une variable peut prendre beaucoup de types appelés *types de base*. Il existe également des *types composés*, mais nous verrons cela ultérieurement. Ces types permettent d'optimiser la consommation de mémoire des programmes, en ne stockant que ce qu'il y a besoin sans surplus (par exemple, inutile d'utiliser un espace de stockage pouvant contenir un nombre allant jusqu'à 4 milliards pour y mettre un nombre allant de 0 à 10).

## Types de nombres

Le stockage des nombres jouit d'une grande diversité de types différents, permettant de stocker des nombres de taille variée. La raison de cette diversité est historique, car les ordinateurs n'ont pas eu tout le temps plusieurs gigaoctets de mémoire vive, permettant de stocker de grands nombres de manière quasi-illimitée. Il y a plusieurs décennies, les ordinateurs ne possédaient que quelques kibioctets de mémoire vive, et à ce moment là, stocker les nombres que l'on utilisait dans nos programmes était une affaire d'optimisation afin de ne pas épuiser le peu de mémoire à laquelle nous avions accès. Depuis, les capacités ont augmenté (de même que la consommation des programmes), mais ces types sont restés pour des raisons de rétro-compatibilité. Néanmoins, il est toujours conseillé d'utiliser les types adaptés pour ne pas gaspiller de mémoire vive. Vala nous propose ces types (avec leur taille indiquée, cette taille indique leur capacité maximale) :

+----------+------------------+----------------------------------------------+-------------------------------------------+
| Type     | Taille (usuelle) | Description                                  | Limites usuelles                          |
+==========+==================+==============================================+===========================================+
| `char`   | 1 octets         | Stocke un nombre entier                      | De -128 à 127                             |
+----------+------------------+----------------------------------------------+-------------------------------------------+
| `short`  | 2 octets         | Stocke un nombre entier                      | De -32 768 à 32 767                       |
+----------+------------------+----------------------------------------------+-------------------------------------------+
| `int`    | 4 octets         | Stocke un nombre entier                      | De -2 147 483 648 à 2 147 483 647         |
+----------+------------------+----------------------------------------------+-------------------------------------------+
| `long`   | 8 octets         | Stocke un nombre entier                      | De -2^63^ à 2^63^ - 1                     |
+----------+------------------+----------------------------------------------+-------------------------------------------+
| `float`  | 4 octets         | Stocke un nombre décimal à virgule flottante | -                                         |
+----------+------------------+----------------------------------------------+-------------------------------------------+
| `double` | 8 octets         | Stocke un nombre décimal à virgule flottante | -                                         |
+----------+------------------+----------------------------------------------+-------------------------------------------+

*Rappel: 1 octet = 8 bits. La capacité de stockage d'un type est donc 2^(taille en bits - 1)^*

[[information]]
| Pour les nombre flottants, nous n'avons volontairement pas indiqué les limites de ces types, car elle est très élevée. Ces nombres, pour représenter les décimaux, utilisent un système proche de la [notation scientifique](http://fr.wikipedia.org/wiki/Notation_scientifique), qui suit la norme [IEEE 754](http://fr.wikipedia.org/wiki/IEEE_754) (vous n'êtes pas obligé de comprendre comment il fonctionne). La capacité de stockage de ces nombres est réellement énorme (la limite supérieure de `float` est dans les 1x10^37^), mais, comme ils ne prennent pas plus de mémoire qu'un `int` et qu'un `long`, cette capacité se fait au détriment de la précision. Faites donc bien attention quand vous manipulez ces types avec de grands nombres ou ayant beaucoup de décimales, ou vous risquez de vous retrouver avec des résultats imprécis dans vos programmes.

En plus des types ci-dessus, il existe deux autres familles de types de variable permettant de stocker des nombres : 

* Tout d'abord, les types à **taille garantie**. Contrairement aux types précédents, dont la taille, et donc les limites, varient d'un système à l'autre (les tailles renseignées étant les tailles usuelles), ces types-ci sont garantis par le système d'avoir la taille indiquée. Ils n'existent que pour les nombres entiers, et portent comme nom **int** (pour *integer*) suivi de la *taille en bits* qui est garantie : *int8*, *int16*, *int32* et *int64*. Leur limite va donc de -2^bits - 1^ à 2^bits - 1^ - 1. Exemple : int16 est limité de -2^15^ (-32 768) à -2^15^ - 1 (32 767).
* L'autre famille est celles des nombres **non-signés**. Pour l'instant, tous les nombres dont nous avons parlé sont des nombres dits **signés**, c'est à dire qu'il possèdent un bit pour indiquer si ils sont négatifs ou pas. Les nombres non-signés, au contraire, ne possèdent pas ce bit, et donc ne peuvent stocker **que des nombres positifs** (supérieurs *ou égaux* à 0). En contrepartie, ils peuvent stocker des nombres deux fois plus grands que leurs équivalents signés ! Ces types s'appellent comme leurs versions signées, à la différence près qu'ils sont préfixés par un « u » Ainsi, `int` devient `uint`, `char` devient `uchar`, et ainsi de suite. Les types *à taille garantie* disposent également de leurs versions non-signées.

## Autres types

Heureusement pour nous, Vala sait traiter d'autres types de variables que les nombres. Ces types de variable représentent des données plus variées que des nombres qui pourront nous aider dans la réalisation de nos programmes :

* Les **booléens**, avec le type `bool`. Derrière ce nom bizarre se cache un type très simple : c'est une variable qui peut prendre deux valeurs : `true` ou `false`. Ceci est très pratique pour traiter avec des données binaires (par exemple, est-ce que la case « J'accepte les conditions d'utilisations » est cochée dans un formulaire).
* Les **chaînes de caractères**, avec le type `string`. Ce type permet de stocker des caractères les uns à la suite des autres, autrement dit... du texte. Sur ce texte vous pourrez effectuer des opérations diverses grâce à une panoplie de fonctions que l'on verra ultérieurement.

# Déclarer une variable.

En Vala, déclarer une variable (« créer » une variable, même si ce n'est pas exactement ça) se fait en spécifiant tout d'abord le type de données qu'elle va contenir. Ensuite, il faut indiquer le nom de la variable, puis enfin optionnellement, sa valeur, séparée par un signe égal : `=`. 

La notation des valeurs est très simple :

* Les **nombres** sont affichables sous plusieurs formes : 
	* sous leur forme normale, mais avec des points à la place des virgules (comme chez les créateurs de la programmation, aux États-Unis) : `123`, `3.14`;
	* en notation scientifique, à la manière d'une calculatrice : `6.02e23` (valable uniquement pour les flottants). Ici c'est comme si on écrivait *6,02x10^23^*, ce qui est après le `e` est donc la puissance de 10;
	* sous forme [octale](http://fr.wikipedia.org/wiki/Syst%C3%A8me_octal) : `0777` (attention quand vous écrivez vos nombres pour ce 0);
	* sous forme [hexadécimale](http://fr.wikipedia.org/wiki/Hexad%C3%A9cimal) : `0xdeadbeef`;
* Le cas spécial du `char` (pour « *character* », caractère en anglais), avec lequel on peut utiliser un caractère directement comme valeur, en l'entourant de guillemets simples `'` : `'a'`. Le code correspondant à ce caractère sera stocké dans la variable. Vous pouvez utiliser également quelques raccourcis pour certains **caractères de contrôle** (des caractères invisibles, mais ayant quand même un effet) : 
	* le retour à la ligne (*line feed*) : `\n`;
	* le retour chariot (*carriage return*) : `\r`;
	* la tabulation : `\t`;
* Les **booléens** sont affichables sous la forme vrai/faux en anglais : `true` et `false`. Cette notation est sensible à la casse (aux majuscules/minuscules), vous ne pouvez donc pas utiliser `True` ou `TRUE`.
* Les **chaînes de caractères** peuvent être indiquées en mettant votre texte entre des doubles guillemets : `"Hello, world !"`. Vous avez déjà utilisé cette notation, pour écrire votre texte "Hello world !" en paramètre de la fonction `print`.

Voici quelques exemples de déclarations de variables de différents types en Vala (j'ai volontairement omis la fonction `main`, je suppose qu'elle encadre tout ce code) :

```vala
int age; // Déclaration de la variable sans assigner de valeur
long zipcode = 75000; // Déclaration de la variable en lui assignant une valeur.

double pi = 3.14; // Les décimaux
double avogadro = 6.0221413e23; // La notation scientifique marche aussi ici, à la manière des calculatrices

uchar niveau = 240; // Variable non signée, peut stocker jusqu'à 255
char nouvelle_ligne = '\n'; // Stockage d'un caractère

bool reponse_valide = true; // Booléen
string pseudo = "johndoe93"; // Stockage d'une chaîne de caractères
```

Si vous voulez déclarer plusieurs variables qui ont le même type, vous pouvez enchaîner les noms de variables, en les séparant par des virgules. Vous pouvez même mêler déclarations avec assignation de valeur et déclaration simple :

```vala
int a, b, c; // On déclare 3 variables sans leur assigner de valeur
string prenom = "Jean", nom_de_famille = "Dupont"; // On déclare 2 chaînes en leur donnant une valeur
float pi = 3.14, e; // On mixe les deux versions ci-dessus.
```

Maintenant que nous avons vu comment Vala stocke des données, et comment déclarer des variables, nous allons pouvoir commencer à les manipuler.