Si vous avez déjà été un peu aventureux, vous avez peut-être déjà essayé d'afficher le contenu d'une variable avec la focntion `print`, mais malheureusement, si vous faites par exemple `print (age)` (la variable age étant celle que nous avons défini plus haut) et que vous essayez de compiler votre programme, vous verrez que le compilateur vous sortira une erreur comme celle-ci :

```
var.vala:4.9-4.11: error: Argument 2: Cannot convert from `int' to `string'
	print (age);
	       ^^^
```

Cette erreur s'affiche car la fonction `print` attend comme qu'on lui donne une *chaîne*, et non pas un *entier*. Cette chaîne de caractères, ce n'est pas juste du texte comme les autres (même si il peut l'être, comme par exemple avec notre "Hello world" réalisé précédemment), c'est une **chaîne de format**. Cela signifie qu'en plus du texte normal, qui sera affiché tel quel, on peut utiliser certains codes spéciaux qui, associés à des paramètres supplémentaires donnés à la fonction `print`, permettront d'afficher nos variables en fonction de leur type. Cela signifie donc que vous ne pouvez pas utiliser n'importe quel type de variable avec n'importe quel code. Ces codes sont les suivants :

+------+------------------------------------------------------------------------------------+
| Code | Explication                                                                        |
+======+====================================================================================+
| %d   | Affiche un nombre entier (requiert un type `int`/`short`/`char`…), mais pas `long` |
+------+------------------------------------------------------------------------------------+
| %ld  | Affiche un un `long`                                                               |
+------+------------------------------------------------------------------------------------+
| %f   | Affiche un nombre décimal (requiert un type `float`)                               |
+------+------------------------------------------------------------------------------------+
| %lf  | Affiche un nombre décimal (requiert un type `double`)                              |
+------+------------------------------------------------------------------------------------+
| %g   | Affiche un nombre décimal de façon intelligente (requiert un `float`)[^g]          |
+------+------------------------------------------------------------------------------------+
| %e   | Affiche un nombre décimal en notation scientifique                                 |
+------+------------------------------------------------------------------------------------+
| %lg  | Affiche un nombre décimal de façon intelligente (requiert `double`)[^g]            |
+------+------------------------------------------------------------------------------------+
| %c   | Affiche un caractère (requiert un type comme `%d`)                                 |
+------+------------------------------------------------------------------------------------+
| %s   | Affiche une chaîne de caractères (requiert un type `string`)                       |
+------+------------------------------------------------------------------------------------+
| %x   | Affiche un nombre entier en hexadécimal minuscule (requiert comme `%d`)            |
+------+------------------------------------------------------------------------------------+
| %X   | Affiche un nombre entier en hexadécimal majuscule (requiert comme `%d`)            |
+------+------------------------------------------------------------------------------------+
| %lx  | Affiche un `long` en héxadécimal minuscule                                         |
+------+------------------------------------------------------------------------------------+
| %lX  | Affiche un `long`  en hexadécimal majuscule                                        |
+------+------------------------------------------------------------------------------------+
| %%   | Affiche un symbole pourcent (`%`)                                                  |
+------+------------------------------------------------------------------------------------+

[[information]]
| La différence entre `%f` et `%g` (et également `%lf` et `%lg`) réside dans le fait que si vous chercher à afficher un nombre décimal avec `%f`, il prendra alors un nombre fixe de chiffres après la virgule pour l'afficher (par exemple, 2.5 sera affiché `2.500000`). Ce n'est pas le cas avec `%g`. Mais des fois, si vous voulez afficher un nombre précis de chiffres après la virgule (par exemple un prix, qui a habituellement 2 chiffres après la virgule), vous pouvez mettre un `0` suivi du nombre de chiffres voulus entre le `%` et le `f` : `%02f` par exemple.

Cette liste n'est bien entendu pas du tout exhaustive et il existe d'autres codes, mais ils ne nous seront sûrement pas utiles dans un premier temps.

Pour revenir sur l'exemple précédent, voici ce que donnerait l'affichage de nos variables avant et après avoir été changées :

```vala
int age;

age = 10;
print ("Vous avez %d ans\n", age); // On affiche notre variable au milieu de texte contextuel

age = 18;
print ("Vous avez maintenant %d ans\n", age); // On affiche l'âge changé
```

Tout de suite, l'utilisation de variables devient plus utile. Mais le plus utile vient quand on lit les données entrées par un utilisateur pour pouvoir les utiliser ensuite.

# Lire des données

Si vous avez déjà programmé, vous devez probablement savoir que récupérer une entrée de l'utilisateur est souvent quelque chose qui est assez ardu à cause des problèmes de sécurité que cela implique. En effet, l'utilisateur peut rentrer ce qu'il veut lorsqu'on lui demande du texte. Pour commencer, nous allons ignorer ces problèmes, et faire une simple lecture qui n'est *pas sécurisée* (ce serait trop compliqué de chercher à faire une lecture sécurisée maintenant).

Pour lire des données, nous auront besoin de la fonction appelée `read_line ()`. Par contre, le fonctionnement de cette focntion est un peu plus complexe que celui de la fonction `print ()` que l'on a déjà vu, vu qu'il est lié à quelque chose d'autre : `stdin`. 

Ce fameux `stdin`, c'est une variable déclarée par défaut par Vala lorsque le programme démarre, et c'est également un **objet**. Nous verrons plus tard en profondeur ce qu'est un *objet* et comment on interagit avec ce type de variable, mais pour l'instant il nous est juste utile de savoir que `stdin` représente **l'entrée standard** de texte. Ce mécanisme est une méthode **d'abstraction** du système d'exploitation pour que nous puissions récupérer du texte de la même manière quelque soit la façon dont il est entré (via un clavier, un clavier visuel, un autre programme...). En vérité, `print ()` fonctionne de la même manière, mais en utilisant une autre variable, `stdout` (ce fonctionnement est celui par défaut, on peut changer la façon dont `print ()` affiche les messages).

Pour lire depuis l'entrée standard, on va utiliser la focntion `stdin.read_line ()`. Cela signifie « utilise la méthode[^meth] `read_line ()` avec comme source `stdin` » (nous verrons plus tard ce qui se passe réellement quand on utilise le `.`).

Pour obtenir ce que l'utilisateur a saisi, il faut assigner la valeur de `read_line` à une variable de type `string`. La valeur de `read_line ()` correspond toujours à ce que l'utilisateur vient d'entrer. Nous verrons bientôt comment marchent les fonctions (et les méthodes) plus en détail, ne vous inquiétez pas si vous ne comprenez pas comment tout ceci est possible. Voici un exemple d'utilisation avec `read_line ()` pour récupérer un nombre entier et une chaîne de caractères :

```vala
print ("Quel est votre nom ?\n");
string nom = stdin.read_line (); // On lit une chaîne de caractères, la lecture se termine quand l'utilisateur tape sur la touche "Entrée"

print ("\nBonjour %s !\n", nom);
```

Cet exemple affiche un message qui demande à l'utilisateur d'entrer son nom, récupère celui-ci *via* `read_line ()`, puis l'affiche avec un message.

# Récupérer autre chose que du texte

Parfois, on peut avoir besoin de récupérer autre chose que du texte, par exemple pour connaître l'âge de l'utilisateur. Il faut alors utiliser la méthode `type.parse (chaine)`, où type est le nom du type que l'on veut obtenir, et `chaine`, la variable contenant l'entrée de l'utilisateur, de type `string`. Par exemple, on peut récupérer l'âge de l'utilisateur avec ce code.

```vala
print ("Entrez votre age : ");

// on veut un int, on utilise int.parse
int age = int.parse (stdin.read_line ()); // On prends directement la valeur de stdin.read_line (), sans variable intermédiaire.

print ("Je suis sur que vous avez %d ans.\n", age)
```

Vous ne pouvez utiliser la méthode `parse` qu'avec les types `bool`, `int`, `double`, `long` et les types de taille garantie, signés ou non-signées (avec un `u` devant).

[^g]: C'est-à-dire qu'il ne va pas afficher de décimales si il n'y en a pas. Il va aussi choisir entre la notation classique (`%f`) et la notation scientifique (`%e`) selon ce qui est le plus approprié.

[^meth]: Une méthode est comme une fonction, à la différence qu'elle s'utilise en combinaison avec un objet. Ici c'est en combinaison avec l'objet `stdin`.