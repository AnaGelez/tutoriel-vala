L'opération la plus essentielle dans un programme, et la plus simple, et d'**assigner** une nouvelle valeur à la variable (on utilise également le terme **affecter**). Bien entendu, il faut que cette variable ait déjà été déclarée, sinon le compilateur vous renverra une erreur. Pour effectuer cela, il suffit d'utiliser le symbole ||=|| (appelé *opérateur*) de la manière suivante :

```vala
int age; // On définit la variable

age = 10; // On assigne une valeur
age = 18; // On réassigne une nouvelle valeur
```

Bien sûr, on peut assigner la valeur d'une variable au contenu d'une autre variable :

```vala
int age = 18;
int nouvel_age = 21;

age = nouvel_age; // On assigne age à la valeur de new_age
```

Pour les valeurs décimales, l'affectation possède un piège : pour pouvoir assigner directement une valeur décimale en dur à un `float`, vous devez lui rajouter un "f" après le nombre que vous souhaitez assigner ou sinon le compilateur vous renverra une erreur. Cela est dû au fait que par défaut, lorsque vous tapez un nombre, le compilateur Vala le considère comme un `double` (car ceux-ci sont plus utilisés), et que comme vous voulez renvoyer cette valeur dans une variable de type `float`, il vous dira que ce n'est pas possible.

```vala
// Ceci ne marchera pas
float pi = 3.14159;

// Par contre, ceci marchera
float pi = 3.14159f;
```

# Les constantes

Les variables, c'est pratique, on peut changer leur valeur comme l'on a envie. Mais des fois, on a pas forcément le besoin ni l'envie de changer la valeur d'une variable après sa déclaration (par exemple, la valeur de Pi, il y a peu de chances qu'elle change au cours de l'exécution de notre programme). Pour cette utilisation, il existe une sorte de variable spéciale : les **constantes**.

Les constantes fonctionnent exactement comme les variables classiques (elles ont un nom et un type soumis aux mêmes règles), sauf que l'on est obligé de définir leur valeur à la création (ou alors le compilateur vous renverra une erreur), et on ne peut pas modifier leur valeur une fois la variable créée (ce qui est le concept principal de la **constante** ne l'oublions pas). Pour définir une variable comme constante, il suffit d'ajouter le mot clé `const` devant son type, comme ça :

```vala
const float PI = 3.14;
```

[[information]]
| Malgré le fait que l'on puisse appeler une constante de la même manière qu'une variable (avec des minuscules notamment), on préfère par convention leur donner des noms entièrement en majuscules, pour ne pas les confondre avec des variables normales.

Pour la petite anecdote, c'est grâce aux constantes que sont définis en Vala un grand nombre de paramètres statiques liés au système qui fait tourner votre programme, ou bien liés à des concepts qui ne risquent pas de changer dans le temps (les constantes mathématiques par exemple).

Maintenant, si vous cherchez à exécuter les programmes précédents, vous remarquerez quelque chose : Il ne se passe rien. Ou plus précisément, il ne se passe rien de *visible*. Pour l'instant, on ne sait pas afficher nos résultats, c'est bien bête. Mais pas de panique, c'est ce qu'on va voir tout de suite.