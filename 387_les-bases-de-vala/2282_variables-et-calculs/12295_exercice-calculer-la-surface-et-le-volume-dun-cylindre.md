Maintenant que nous avons vu les notions de bases concernant les variables (d'autres notions vont venir plus tard, qui nous permettront de réaliser des programmes plus complexes), nous allons mettre tout ça en application avec un petit exercice.

Celui-ci est simple, il suffit de faire un programme qui calcule la surface et le volume d'un cylindre aux dimensions rentrées par l'utilisateur, puis affiche le résultat d'une manière claire. Ce programme devra ressembler à ça :

-> ![Screenshot résultat exercice cylindre.](http://zestedesavoir.com/media/galleries/487/c7347c0f-aeef-4003-8a68-a1b58369937a.png.960x960_q85.png) <-

Si vous ne vous en rappelez pas, voici les deux formules de calcul dont vous aurez besoin pour terminer votre programme. Dans ces formules, `r` représente le rayon du cercle représentant la base du cylindre, et `h` la hauteur du cylindre :

* Formule pour calculer l'aire d'un cylindre : $2 \pi \times r \times h$
* Formule pour calculer le volume d'un cylindre : $\pi \times r^2 \times h$

Pour ce programme, une précision absolue n'est pas forcément nécessaire, mais nous vous conseillons d'utiliser des `double` à la place de `float` comme type de variable (vous êtes obligés d'utiliser des types flottants et non entiers, sinon calculer pour des dimensions entrées avec des nombres décimaux ne marchera pas), aussi parce qu'on les créer depuis un texte rentré par l'utilisateur.

Bon, voilà, j'ai tout dit pour vous aider sur cet exercice, vous pouvez maintenant essayer de le faire.

# Solution

Nous ne vous conseille pas de lire cette solution avant d'avoir au moins essayé de faire cet exercice de vous-même. Cette solution est bien évidemment commentée pas à pas pour vous permettre de comprendre tout le fonctionnement de ce programme.

[[secret]]
| Pour ma solution, j'ai préféré utiliser des `double`, par habitude et aussi parce qu'on peut utiliser `parse` avec eux. Par contre, cela implique que j'utilise `%lg` pour l'affichage au lieu de simplement `%g`.
|
| ```vala
| void main ()
| {
| 	// On aura besoin de pi comme constante
| 	const double PI = 3.14159;
| 	
| 	// On affiche un titre et une invite pour rentrer les valeurs
| 	print ("--- Calcul de surface/volume d'un cylindre ---\n");
| 	print ("Veuillez indiquer le rayon de la base, ainsi que la hauteur du cylindre :\n\n");
| 	
| 	// Récupération des données avec des read_line
| 	print ("Rayon de la base : ");
| 	double rayon_base = double.parse (stdin.read_line ());
| 	print ("Hauteur : ");
| 	double hauteur = double.parse (stdin.read_line ());
| 	
| 	// Le calcul, pas trop compliqué
| 	double aire = (2 * PI) * rayon_base * hauteur;
| 	double volume = PI * (rayon_base * rayon_base) * hauteur;
| 	
| 	// Enfin, on affiche les résultats
| 	print ("\nAire du cylindre : %lg\n", aire);
| 	print ("Volume du cylindre : %lg\n", volume);
| }
| ```
