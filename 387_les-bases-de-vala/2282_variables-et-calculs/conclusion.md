# Ce qu'il faut retenir

- Pour stocker et manipuler des données, nous utiliseront des variables.
- Il existe des types de variables différents selon ce qu'on veut y stocker : `int` ne sert que pour des nombres entiers, `double` pour des nombres à virgule, `string` pour du texte ...
- Une variable est identifiée par un nom qui permet de retrouver ou de modifier sa valeur.
- On peut utiliser des constantes avec le mot-clé `const`. Elles servent à stocker des valeurs qui ne changeront jamais.
- On peut afficher des messages avec la méthode `print ()`.
- On peut lire des messages avec la méthode `stdin.read_line ()`.
- On peut faire des calculs très facilement, avec les symboles `+`, `-`, `*`, `/` et `%`.

Ce chapitre fut plutôt long et ennuyeux (beaucoup de notions théoriques pour finalement pas grand chose), mais il est très important pour la suite, les variables étant une notion essentielle dans la programmation. Dans le prochain chapitre, nous verrons une autre notion essentielle, mais cette fois-ci bien plus intéressante, car elle nous apprendra à faire des choses intéressantes avec notre programme !