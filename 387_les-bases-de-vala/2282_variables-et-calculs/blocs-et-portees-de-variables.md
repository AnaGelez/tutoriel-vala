TODO: déplacer ça après, dans le chapitre sur les conditions pour que la notion de blc soit moins abstraite.

Vous allez très vite vous rendre compte qu'un programme n'est qu'un emboîtement de briques logiques. Prenons par exemple cet algorithme qui permettrait de guider un robot le long d'un chemin truffé d'obstacles.

```
Tant que le robot n'est pas arrivé
    Si il y a un obstacle
        Contourner l'obstacle
    Avancer
```

On peut le diviser en deux blocs logiques imbriqués : le `Tant que` et le `Si` (les deux autres lignes étant des instructions dans ces blocs).

En Vala, les blocs sont délimités par des accolades : `{` pour ouvrir et `}` pour fermer et contiennent plusieurs instructions ou d'autre blocs dans l'ordre dans lequel ils devront être exécutés. En général les blocs sont précédés d'un élément de logique (comme le `Tant que` de notre exemple), mais on peut très bien en écrire sans rien de plus que les accolades.

```
{
    print ("Salut");
}
```

[[information]]
| La fonction `main` que vous avez écrit est donc un bloc.

Un bloc comme celui qu'on vient de voir ne sert pas à grand chose. Mais si nous vous en parlons maintenant c'est pour parler de portée de variables et d'indentation.

# Portée des variables

Lorsqu'on déclare une variable, celle ci n'est disponible que dans le bloc où elle a été déclaré ainsi que les blocs « enfants » (ceux qui sont eux-mêmes contenus dans ce bloc). Ce code est par exemple invalide.

```vala
{
    int score = 7852;
}
print ("Bravo, votre score est de %d !\n", score);
```

Mais celui-ci fonctionne très bien.

```vala
int score = 7852;
{
    print ("Bravo, votre score est de %d !\n", score);
}
```

Si vous essayez le premier programme vous devriez obtenir une erreur qui ressemble à celle-ci.

[[erreur]]
| The name `score` does not exist in the context of `main'

# Indentation

Vous avez peut-être remarqué que nous décalions notre code à l'intérieur des blocs. C'est ce qu'on appelle l'**indentation**. On distingue ainsi clairement là où commencent et finissent les différents blocs.

On peut soit utiliser des espaces (deux ou quatre en général) ou des tabulations pour indenter son code. L'important est de rester cohérent et de toujours indenter de la même façon dans tout votre code.

Les niveaux d'indentation s'ajoutent les uns aux autres. Si vous avez deux blocs imbriqués vous décalerez plus dans le second que dans le premier.

```vala
void main () {
    print ("Indenté avec quatre espaces.\n");
    {
        print ("Indenté avec deux fois quatre espaces.\n");
    } 
}
```