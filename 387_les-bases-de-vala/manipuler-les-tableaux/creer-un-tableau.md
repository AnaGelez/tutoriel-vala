Un tableau ne peut contenir qu'un certain type de variables. Vous imaginez bien que sinon ça serait un peu le bazar, avoir des nombres mélangés à du texte et des booléens, sans savoir qui correspond vraiment à quoi ...

Pour créer un tableau, on va faire exactement comme pour une variable normale, on va juste rajouter des crochets après le type de notre variable.

```vala
int[] mon_tableau; // J'ai pris des entiers, mais ça marche avec n'importe quel type
```

Seulement, si on fait ça notre tableau ... ne contient rien. On va donc l'initialiser. Pour initaliser un tableau il faut mettre les valeurs qu'il contient à la suite, séparées par des virgules, le tout entre crochets.

```vala
int[] mon_tableau = { 3, 7, 42, 666 };
```

Et voilà, vous savez initialiser des tableaux. Et maintenant, on va voir comment lire et changer les valeurs qu'il contient.

# Accéder aux éléments

Chaque élément d'un tableau est associé à un entier, qu'on appelle **index**. On peut retrouver un élément en demandant l'index qui lui correspond. Pour récupérer un index, on utilise la syntaxe suivante.

```vala
int element = mon_tableau [index];
```

[[information]]
| Ici, les crochets sont littéralement là.

L'index peut tout aussi bien être un entier écrit directement, qu'une variable de type `int`. Il faut aussi savoir que pour récupérer le premier élement, il faut demander l'index 0, pas l'index 1.

```vala
int[] mon_tableau = { 3, 7, 42, 666 };
int premier = mon_tableau[0];
print ("Le premier element est %d.\n" premier);

/* On verra :
Le premier element est 3.
*/
```

[[information]]
| Pour connaître le nombre d'élément d'un tableau, vous pouvez utiliser `mon_tableau.length`. Vous pourrez ainsi vérifier que vous ne prenez pas un élément qui n'existe pas (dans le cas où l'utilisateur aurait choisi l'index par exemple).

# Récupérer un morceau de tableau

Vous pouvez aussi récupérer une partie d'un tableau en indiquant entre crochet l'index de début et l'index de fin (inclus), séparés par `:`. Vous pouvez l'assigner à un nouveau tableau du même type que l'original.

```vala
int[] mon_tableau = { 3, 7, 42, 666 };
int[] debut = mon_tableau [0:1];
// debut contient 3 et 7

int[] fin = mon_tableau [1:3];
// fin contient 7, 42 et 666.
```
[[attention]]
| Si vous modifiez par la suite des éléments communs à un des deux tableaux, les changements auront aussi lieu dans l'autre.

# Modifier les éléments

Heureusement, les tableaux, ne sont pas figés (à moins qu'ils ne soient des constantes). On peut modifier la valeur contenue à un index précis, comme si c'était une variable classique.

```vala
int[] mon_tableau = { 3, 7, 42, 666 };
mon_tableau[0] = 55;
```

La variable `mon_tableau` contiendra désormais 55, 7, 42 et 666.

Et ce n'est pas tout ! On peut tout à fait y ajouter des éléments à un tableau en utilisant l'opérateur `+=`. Bien-sûr, il faut que l'élément qu'on ajoute soit du bon type, le même que celui du tableau.

```vala
int element = 12;
mon_tableau += element;
```

La valeur de la variable `element` sera alors ajoutée à la fin du tableau. Malheureusement, il n'existe pas de syntaxe similaire pour enlever un élément d'un tableau. Pour faire cela on devra utiliser quelque chose qui ressemble à ce code.

```vala
// TODO : tester ce code
int[] tableau = {1, 2, 4, 5, 10};
int[] nouveau = {};
int a_enlever = 4;

for (int i = 0; i < tableau.length; i++) {
    if (tableau[i] != a_enlever) {
        nouveau += tableau[i];
        print ("On ajoute %d.\n", tableau[i]); 
    } else {
        print ("On laisse %d.\n", tableau[i]);
    }
}
```

Ce qui est un peu plus ... verbeux.