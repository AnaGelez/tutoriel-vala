En Vala, les tableaux sont ce qu'on pourrait appeler dans un langage commun une liste.[^list] Ce sont des variables un peu spéciales qui en contiennent en réalité plusieurs. On va donc pouvoir avoir une série de nombres, de textes, de booléens ou autre. On va pouvoir ajouter, modifier ou utiliser ces éléments et les parcourir un à un pour faire des traitements sur des données en boucle.

Les tableaux nous permettent donc de manipuler des séries de données. On va par exemple pouvoir lister des personnes, afficher un menu de restaurant, gérer une liste de tâches, ou pour reprendre un précédent exemple, afficher les réponses au Tweet posté.

[^list]: En Vala, on parle aussi de listes, mais ce n'est pas vraiment la même chose.