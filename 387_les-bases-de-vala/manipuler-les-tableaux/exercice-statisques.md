Et maintenant l'heure du petit exercice de fin de chapitre est arrivée ! :pirate:

Nous allons créer un petit programme capable de calculer différentes valeurs en rapport avec une série de nombres.

Il faudra que notre application puisse calculer la moyenne, la médiane et l'étendue de la série. On va vous rappeller rapidement comment calculer tout ça si vous avez oublié. Pour la moyenne, il faut faire la somme de tous les éléments de la série, puis diviser le résultat par le nombre d'éléments. Pour la médiane, il faut prendre l'élément qui sépare la série en deux moitiés, une avec les éléments plus petits, l'autre avec ceux qui sont plus grands. Si il y a un nombre pair d'éléments dans la série, on prends la moyenne des deux éléments centraux. Pour l'étendue, il suffit de calculer la différence entre le plus grand élément et le plus petit.

Attention cependant, pour vous compliquer un peu la tâche, nous vous imposons d'utiliser cette série, dans cet ordre : 5, 7, 48, 59, 44, 7, 68, 125, 96 et 2.

Vous aurez aussi besoin de trier la série pour calculer la médiane, pour cela il faudra écrire cette fonction :

```vala
void ordonner (ref int[] serie) {
    // algorithme appelé "bubble sort"
    // vous n'êtes pas obligés de comprendre ce qui se passe ici,
    // même si vous pouvez toujours essayer de le faire.
    int n = serie.length;
    for (int x = 0; x < n; x++) {
        for (int y = 0; y < n - 1; y++) {
            if (serie[y] > serie[y + 1]) {
                int temp = serie[y + 1];
                serie[y + 1] = serie[y];
                serie[y] = temp;
            }
        }
    }
}
```

Vous pourrez l'utiliser comme ceci : `ordonner (ref serie);`.

Et si possible, utilisez des fonctions, vous verrez que ça vous sera utile, notamment pour la moyenne. ;)

## Correction

Avant de vous jeter sur la correction, essayez quelque chose, c'est comme ça que vous progresserez le plus rapidement. Pour la petite anecdote, j'ai lu plusieurs tutoriels sur des langages différents quand j'ai découvert le monde de la programmation, mais sans jamais faire les exercices pratiques. Résultat, je ne maîtrise pas ces langages, et j'ai perdu des heures en voulant gagner quelques minutes à ne pas m'entraîner.

En tout cas, que vous ayez trouvé une solution ou non, en voici une possible.

[[secret]]
| ```vala
| void main () {
|     // notre série est un tableau d'entiers
|     int[] serie = { 5, 7, 48, 59, 44, 7, 68, 125, 96, 2};
| 
|     // on ordonne la série, sinon le calcul de la médiane sera faux.
|     ordonner (ref serie);
| 
|     // on calcule la moyenne ...
|     print ("La moyenne est %g.\n", moyenne (serie));
| 
|      // la médiane ...
|      print ("La médiane est %g.\n", mediane (serie));
| 
|      // et l'étendue
|      print ("L'étendue est %d.\n", etendue (serie));
| }
| 
| double moyenne (int[] serie) {
|      int total = 0;
|      // on fait la somme de tous les chiffres de la série
|      foreach (int x in serie) {
|           total += x;
|      }
|      // on divise cette somme par le nombre d'éléments
|      return total / (double) serie.length;
| }
| 
| double mediane (int[] serie) {
|      // si le nombre d'éléments est pair (le reste de la division par 2 est 0)
|      if (serie.length % 2 == 0) {
|      // on détermine les deŭ éléments centraux
|           int centre1 = serie[(serie.length - 1) / 2];
|           int centre2 = serie[(serie.length - 1) / 2 + 1];
|           // on en fait la moyenne
|           return moyenne ({ centre1, centre2 });
|      } else {
|           // si il est impair, on prends le nombre du milieu
|           int centre = serie[(serie.length - 1) / 2 + 1];
|           return (double) centre;
|      }
| }
| 
| int etendue (int[] serie) {
|      // soustraction du plus grand élément et du plus petit
|      return  serie[serie.length - 1] - serie[0];
| }
| 
| void ordonner (ref int[] serie) {
|      // algorithme appelé "bubble sort"
|      // vous n'êtes pas obligés de comprendre ce qui se passe ici,
|      // même si vous pouvez toujours essayer de le faire.
|      int n = serie.length;
|      for (int x = 0; x < n; x++) {
|           for (int y = 0; y < n - 1; y++) {
|                if (serie[y] > serie[y + 1]) {
|                     int temp = serie[y + 1];
|                     serie[y + 1] = serie[y];
|                     serie[y] = temp;
|                 }
|           }
|      }
| }
| ```

Vous pouvez vous amuser à modifier la série, le code fonctionnera toujours !

Et comme toujours : même si votre solution ne ressemble pas à ça, du moment que ça marche et que votre code est compréhensible, il n'y a pas de problèmes !