On sait récupérer un élément à un index donné. On sait récupérer la longueur d'un tableau. On pourrait très facilement écrire un petit code pour parcourir notre tableau, non ? :D

# Avec la boucle for

La boucle for est assez pratique pour parcourir un tableau, puisque sa syntaxe nous permet d'initialiser un entier qui nous permettra ensuite de récupérer l'élément à cet index. On aura plus qu'à incrémenter cet entier tant qu'il ne dépasse pas la taille du tableau, et le tour est joué.

On pourrait alors avoir ce code.

```vala
int[] mon_tableau = { 3, 7, 42, 666 };
for (int i = 0; i < mon_tableau.length; i++) {
    int actuel = mon_tableau [i];
    print ("L'element a l'index %d est %d.\n", i, actuel);
}
```

Si on compile et exécute ce programme, on obtiendra ...

```
L'element a l'index 0 est 3.
L'element a l'index 1 est 7.
L'element a l'index 2 est 42.
L'element a l'index 3 est 666.
```

Cependant, il existe une autre boucle que vous ne connaissez pas encore qui nous permet d'arriver au même résultat plus simplement ...

# Une nouvelle boucle : foreach

Vous ne connaissez pas encore la boucle `foreach`, et c'est normal : elle n'est utilisable que pour parcourir des tableaux, on ne l'a donc pas vu lors du chapitre sur les boucles.

Cette boucle nous offre une syntaxe bien plus agréable que la boucle `for` pour parcourir les tableaux. Elle ressemble à ceci.

```vala
foreach ([type] [nom] in [tableau]) {
    // ...
}
```

Ici, `type` est le type de la variable qui contiendra l'élément actuel. C'est donc le même que celui du tableau. `nom` est le nom de la variable qui contiendra l'élément actuel. Et `tableau` est bien-sûr le tableau à parcourir.

On peut ensuite utiliser la variable qui contient l'élément actuel comme n'importe quelle autre, à l'intérieur du bloc de la boucle.

```vala
int[] mon_tableau = { 3, 7, 42, 666 };
foreach (int actuel in mon_tableau) {
    print ("L'élément actuel est %d.\n", actuel);
}
```

L'un des désavantage de cette boucle est tout de même qu'elle ne nous donne pas de variable semblable à `i` dans la boucle `for` que nous avions fait plus haut. On peut cependant facilement palier à ce problème en initialisant `i` avant la boucle et en l'incrémentant à chaque tour, comme on ferait dans un `while`.
